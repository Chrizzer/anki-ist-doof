exports.shouldUpdateScroll = ({
  routerProps: { location },
  getSavedScrollPosition,
}) => {
  const currentPosition = getSavedScrollPosition(location)
  // scoll to bottom
  if (window["__whmEventSourceWrapper"]) {
    window.scrollTo(...(currentPosition || [0, window.screen.height]))
  } else {
    window.scrollTo(...(currentPosition || [0, 0]))
  }

  return false
}
