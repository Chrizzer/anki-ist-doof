import React from "react"
import "./main.css"
import { Link} from "gatsby"

export default class Main extends React.Component {
  render() {
    return (
      <div id="main">
        <h1>What is this?</h1>
        <p>
          I made this website for myself. Its main purpose is, to be a
          collection of all relevant information for my current studies at
          University of Lübeck. Also, its written in german. <br />
          In an effort to continuously learn throughout the semester, my goal is
          to make a framework to quickly add new information from lectures for
          fast and easy access.
        </p>
        <div className="stundenplan">
          {this.props.semesters.map((semester, i) => (
            <div key={i} className="semester">
              <div className="title">
                <span>Semester {i + 1}</span>
              </div>
              {semester.map((v, j) => (
                <Link
                  key={j}
                  to={"semester/" + (i + 1) + "/" + v}
                  className="item"
                >
                  <span>{v}</span>
                </Link>
              ))}
            </div>
          ))}
          <div className="semester">
            <div className="title">
              <span>Sonstiges </span>
            </div>
            {this.props.others.map((v, i) => (
              <Link key={i} to={"other/" + v} className="item">
                <span>{v}</span>
              </Link>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
