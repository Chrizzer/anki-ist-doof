import React from "react"
import "./style.css"

export function separateChildrenProps(children) {
  let examples = []
  if (children instanceof Array) {
    for (let i = 0; i < children.length; i++) {
      examples.push(
        <>
          <span>{children[i]}</span>
          {i + 1 < children.length && <br />}
        </>
      )
    }
  } else {
    examples.push(children)
  }
  return examples
}

export default class Example extends React.Component {
  render() {
    let examples = separateChildrenProps(this.props.children)
    const name = "Beispiel"
    if (this.props.title) {
      return (
        <div className="def-container example-layout">
          <div className="def-header">
            <span>
              {name} ({this.props.title})
            </span>
          </div>
          <div className="def-body">{examples}</div>
        </div>
      )
    } else {
      return (
        <div className="example-container">
          <b>{name}:</b>
          <div className="example-body">{examples}</div>
        </div>
      )
    }
  }
}
