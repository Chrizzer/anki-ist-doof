import React from "react"
import { Helmet } from "react-helmet"
import "./style.css"
import { Link, graphql } from "gatsby"

export default function Deck(props) {
  return (
    <Link className="deck" to={`decks/${props.name}`} state={{}}>
      <span className="center">{props.name}</span>
      <span className="counter">{props.number} Cards</span>
    </Link>
  )
}
const query = graphql`
  query cards($deck: String) {
    allCard(filter: { deck: { eq: $deck } }) {
      nodes {
        answer
        question
      }
    }
  }
`
