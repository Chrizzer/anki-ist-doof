import React from "react"
import "./style.css"
import Card from "../card/card_parsed"

export default class LiveEditor extends React.Component {
  state = {
    txt: "Type something $\\frac{a}{b}$",
  }

  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.setState({
      txt: event.target.value,
    })
  }

  render() {
    return (
      <div className="main">
        <h1 className="header">Live Editor</h1>
        <div className="preview">
          <Card question={[this.state.txt]} />
        </div>
        <div className="editor">
          <textarea
            className="jsonResult"
            readOnly={true}
            wrap="off"
            rows="1"
            onClick={e => e.target.select()}
            value={this.state.txt.replace(/\\/g, "\\\\")}
          />
          <textarea
            className="textfield"
            value={this.state.txt}
            onChange={this.handleChange}
          />
        </div>
      </div>
    )
  }
}
