import React from "react"
import "prism/prism.css"
// import { render } from "react-dom"
import Prism from "prism/prism.js"
import Highlight, { defaultProps } from "prism-react-renderer"
import { LiveProvider, LiveEditor, LiveError, LivePreview } from "react-live"
import "./style.css"

export default ({ language, ...props }) => {
  if (props.className) {
    language = props.className.split("-")[1]
  }
  if (false && props["live"]) {
    return (
      <div className="code-container">
        <LiveProvider code={props.children} noInline={true}>
          <LiveEditor />
          <LiveError />
          <LivePreview />
        </LiveProvider>
      </div>
    )
  } else {
    return (
      <Highlight
        Prism={Prism}
        {...defaultProps}
        code={props.children}
        language={language}
      >
        {({ className, style, tokens, getLineProps, getTokenProps }) => (
          <pre className={className} style={style}>
            {tokens.map((line, i) => (
              <div {...getLineProps({ line, key: i })}>
                {line.map((token, key) => (
                  <span {...getTokenProps({ token, key })} />
                ))}
              </div>
            ))}
          </pre>
        )}
      </Highlight>
    )
  }
}
