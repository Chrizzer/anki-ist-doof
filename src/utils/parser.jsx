import React from "react"
import Image from "../components/image/image.fluid"

export default function parse(props) {
  return (
    <>
      <div className="card">
        {props.topic && topic(props.topic)}
        {(props.question || props.image || props.img || props.snippet) && (
          <div
            style={{ display: !props.showAnswer ? "flex" : "none" }}
            className="front"
          >
            {props.question && question(props.question)}
            {props.image &&
              props.image.position !== "back" &&
              image(props.image)}
            {props.img && img(props.img)}
            {props.snippet && snippet(props.snippet)}
          </div>
        )}
        {props.answer && (
          <div
            style={{ display: props.showAnswer ? "flex" : "none" }}
            className="back"
          >
            {answer(props.answer)}
            {props.image &&
              props.image.position === "back" &&
              image(props.image)}
          </div>
        )}
      </div>
    </>
  )
}

function makeContent(str) {
  return str.split(/\\([^{]]*{[^}]*})/).map((token, i) => {
    if (token.startsWith("b")) {
      return <b key={"bold" + i}>{token.substring(2, token.length - 1)}</b>
    } else {
      return token
    }
  })
}

function question(question) {
  return (
    <div className="container-q">
      <span className="question">
        {question.map((line, i) => (
          <>
            {makeContent(line)}
            <br />
          </>
        ))}
      </span>
    </div>
  )
}

function answer(answer) {
  return (
    <div className="container-a">
      <span className="answer">
        {answer.map((line, i) => (
          <>
            {makeContent(line)}
            <br />
          </>
        ))}
      </span>
    </div>
  )
}

function topic(name) {
  const content = makeContent(name)
  return (
    <div className="container-t">
      <h2>{content}</h2>
    </div>
  )
}

function image(obj) {
  const { path, caption } = obj
  return (
    <figure className="container-img">
      <Image imgName={path} />
      {caption && <figcaption>{makeContent(caption)}</figcaption>}
    </figure>
  )
}

function img(path) {
  return (
    <div className="container-img">
      <Image imgName={path} />
    </div>
  )
}

function snippet(code) {
  return (
    <div className="container-code">
      {code.map((line, i) => (
        <pre key={"pre" + i}>
          {i + "  "}
          {line}
        </pre>
      ))}
    </div>
  )
}
