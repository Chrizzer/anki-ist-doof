import "../../mdx.css"
import Layout from "components/default-page-layout.jsx"

import Image from "components/image/image.fluid"

export default ({ children }) => (
  <Layout id="mdx" headerName="Datenbanken">
    {children}
  </Layout>
)

## Entity-Relationship Diagram

### Kardinalitäten

<Image imgName="db-notation.png" />

### Typen und Werte

<Image imgName="db-erm-1.png" />

#### Aggregation und Komposition

<div style={{"display":"flex"}}>

<div style={{ margin: "4px", flex: "1" }}>
  <Image imgName="db-erm-overlapping.png" />
</div>

<div style={{ margin: "4px", flex: "1" }}>
  <Image imgName="db-erm-disjoint.png" />
</div>

</div>

<Image imgName="db-partizipation.png" />

### Modellierung von Relationen

$$
R: E_1 \to E_2
$$

> R ist eine Relation
>
> Elemente $y \in E_2$ können ein Urbild $x \in E_1$ haben

<Image imgName="db-erm-relation.png" />

> R ist eine partielle surjektive Funktion
>
> Alle Elemente $y \in E_2$ haben mindestens ein Urbild $x \in E_1$ haben

<Image imgName="db-erm-relation-2.png" />

> R ist eine bijektive Funktion
>
> Alle Elemente $y \in E_2$ haben mindestens ein Urbild $x \in E_1$ haben

<Image imgName="db-erm-relation-3.png" />

## Relationale Algebra

$$
\begin{align\*}
\text{Projektion} &= \pi \\\\
\text{Selektion} &= \sigma_\theta(R) \\\\
\text{Vereinigung} &= R \cup S \\\\
\text{Differenz} &= R \setminus S \\\\
\text{Durchschnitt} &= R \cap S \\\\
\text{Kartesisches Produkt} &= R \times S \\\\
\text{Join über Prädikat} &= R \Join_\theta S \\\\
\end{align\*}
$$

## Structured Query Language

```sql
CREATE TABLE Tbl_A (
    ID int NOT NULL, // NAME TYPE [CONSTRAINTS]
    fk int NOT NULL,
    age int,
    PRIMARY KEY (ID),
    FOREIGN KEY (fk) REFERENCES Tbl_B(ID),
    CONSTRAINT isAdult CHECK (age >= 18) // Tabellenzusicherung #1
)
// Tabellenzusicherung #2
ALTER TABLE Tbl_A ADD CONSTRAINT positive
CHECK (age >= 0)
// Domänenzusicherung POSTGRE SQL
CREATE DOMAIN AGE AS INT
CHECK (value >= 0)
```

## Indexed Sequential Access Method

> **Degeneration eines Index** tritt auf, wenn die sequentielle Ordnung der Blattknoten zerstört wird. Einfügen von neuen Datensätzen, welche in Überlaufseiten gesetzt werden müssen führen dazu.

- Nützlich für (relativ) statische Daten
- Löschen ist trivial: Datensatz wird einfach gelöscht
- Einfügen ist aufwendig: Falls genug Platz ist, einfach einfügen. Sonst auf neuer Überlauf-Seite einfügen.
- Knoten enthalten Referenzen oder tatsächliche Daten

### B$^+$-Baum

- Basiert auf ISAM
- Balancierung bei Einfügen und Löschen wird aufrechterhalten
- Knoten müssen zwischen $d$ und $2d$ Einträge enthalten (Ausnahme Wurzel)
- Blätter werden in doppelt verketteter Liste verbunden
- Daten können wahlfrei verteilt sein

#### Einfügen

<div style={{"display":"flex"}}>

<div style={{ margin: "4px", flex: "1" }}>
  <Image imgName="db-b+_insert.png" />
</div>

<div style={{ margin: "4px", flex: "1" }}>
  <Image imgName="db-b+_insert2.png" />
</div>
</div>

#### Löschen

- Falls Anzahl Daten in Knoten weniger als $d/2$ ist, dann
  1. Borrow from left
  2. Merge with left
  3. Borrow from right
  4. Merge with right
- Innere Knoten borrown komplette Kinder
- Blätter nur Datenpunkte
- Falls die Wurzel nur ein Kind hat, wird das Kind die neue Wurzel

## Magnetische Festplatte

<Image imgName="db-hdd.png" />

## Transaktionen

> Eine Transaktion ist eine Folge von Schritten. Schritte sind Zugriffsoperationen auf ein/e Objekt/Entität.
>
> Transaktion $T = \langle s_i,\dots,s_n \rangle$
>
> Schritt $s_i = op(e_i)$
>
> Operation $op \in \\{ \text{r(ead)},\text{w(rite)} \\}$
>
> Länge $|T| = n$ ist Anzahl der Schritte

### ACID

#### Atomicity

Entweder werden **alle** oder **keine** Werteänderungen einer Transkation übernommen.

#### Consistency

Eine Transaktion überführt einen konsistenten Zustand in einen anderen.

#### Isolation

Eine Transaktion berücksichtigt keine Effekte parallel laufender Transaktionen.

#### Durability

Effekte einer erfolgreichen Transaktion werden persistent gemacht.

### Anomalien

#### Lost Update

Effekte einer Transaktion gehen verloren, weil eine andere Transaktion Werte überschreibt

$$
\begin{array}{c|c}
T1 & T2 \\\\ \hline
\text{write}(A) &  \\\\
 & \text{write}(A) \\\\
\end{array}
$$

#### Dirty Read

Eine Transaktion verwendet einen falschen Wert, weil eine andere Transaktion diesen schon überschrieben hat.

$$
\begin{array}{c|c}
T1 & T2 \\\\ \hline
\text{write}(A) &  \\\\
 & \text{read}(A)
\end{array}
$$

#### Inconsistent Read

Eine Transaktion verwendet einen Wert, der von einer anderen Transaktion überschrieben wird.

$$
\begin{array}{c|c}
T1 & T2 \\\\ \hline
\text{read}(A) &  \\\\
 & \text{write}(A)
\end{array}
$$

#### Deadlock

Transaktionen warten gegenseitig auf ein Unlock.

$$
\begin{array}{c|c}
T1 & T2 \\\\ \hline
\text{lock}(A) & \text{lock}(B) \\\\
\text{lock}(B) & \text{lock}(A) \\\\
\text{unlock}(A) & \text{unlock}(B)
\end{array}
$$

### Sequentieller Plan

Ein Plan der alle Schritte aus Transaktionen enthält. Die Ordnung der Schritte muss erhalten bleiben.

Ein Plan heißt seriell genau dann, wenn für jede Transaktion $T_j$ alle ihre Schritte direkt aufeinanderfolgen

Jede serielle Ausführung ist korrekt.

#### Konflikt

Ein Konflikt ensteht nur durch zwei Operationen auf einem gleichen Objekt, falls eine Operation eine Schreiboperation ist.

### Transaktionsverwaltung

Über lock und unlock können Transaktionen Relationen (Tabellen) für alle anderen Transaktionen blocken. So können viele Konflikte gelöst werden.

Es werden zwischen Read-Lock und Write-Lock unterschieden:

> Read-Lock: Mehrere Transaktionen können lesend auf Tabellen zugreifen
>
> Write-Lock: Nur die sperrende Transaktion kann auf Tabelle zugreifen und schreiben

#### 2-Phasen-Sperrverwaltung

Transaktionen dürfen erst nach einem Unlock, einen neuen Lock anfordern. Also darf eine Transaktion niemals zwei Locks gleichzeitig setzen.

> **Konservatives** 2PL: Zu Beginn einer Transaktion werden alle Locks gesetzt. Deadlocks werden so verhindert, Parallelität geht verloren.
>
> **Striktes** 2PL: Write-Locks werden erst am Ende einer Transaktion freigegeben. Read-Locks gibt es nicht in der strikten Version.
