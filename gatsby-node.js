// gatsby-node.js
const path = require(`path`)
const fs = require("fs")
const inspect = require("util").inspect

const createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  return (
    new Promise(resolve => {
      resolve(
        // Query all Cards from JSon
        graphql(`
          query decksWithCards {
            allDeck {
              nodes {
                name
                cards {
                  answer
                  question
                  snippet
                  topic
                  img
                  image {
                    position
                    caption
                    path
                  }
                }
              }
            }
          }
        `)
      )
    })
      // .then(result => {
      //   // Load Template for new Pages
      //   const browser = path.resolve(`src/components/browser/browser.jsx`)
      //   // Create Pages for each deck
      //   result.data.allDeck.nodes.forEach(deck => {
      //     createPage({
      //       path: `decks/${deck.name}`,
      //       component: browser,
      //       context: {
      //         ...deck,
      //       },
      //     })
      //   })
      // })
      // .then(() => {
      //   const editor = path.resolve(`src/components/live_editor/live_editor.jsx`)
      //   createPage({
      //     path: "editor",
      //     component: editor,
      //     context: {},
      //   })
      // })
      .catch(error => {
        console.log(error)
      })
  )
}
// GraphQL Node Type declarations
exports.sourceNodes = ({ actions, createNodeId, createContentDigest }) => {
  const { createTypes, createNode } = actions
  const typeDefs = `
    # One must say that the type is a Node
    type Deck implements Node {
      name: String
      cards: [Card]
    }
    type MyImage {
      path: String!
      position: String # front, back
      caption: String
    }
    type Card implements Node @dontInfer{
      deck: String
      question: [String]
      answer: [String]
      snippet: [String]
      topic: String
      img: String
      image: MyImage
    }
  `
  createTypes(typeDefs)
  const decksPath = path.resolve(`src/data/decks/`)
  // Load data and create nodes
  fs.readdirSync(decksPath).map(fileName => {
    if (fileName.endsWith(".json")) {
      const deckName = fileName.substr(0, fileName.length - 5)
      const fileContent = JSON.parse(
        fs.readFileSync(path.resolve(decksPath, fileName))
      ).map((card, i) => {
        let cardNode = {
          // data fields
          ...card,
          deck: deckName,
          // required fields
          id: createNodeId(fileName + i),
          parent: null,
          children: [],
          internal: {
            type: "Card",
            contentDigest: createContentDigest(card),
            // mediaType, content, description are optional
          },
        }
        createNode(cardNode)
        return cardNode
      })
      let content = {
        name: deckName,
        cards: fileContent,
      }

      let deckNode = {
        // data fields
        ...content,
        // required fields
        id: createNodeId(fileName),
        parent: null,
        children: [],
        internal: {
          type: "Deck",
          contentDigest: createContentDigest(content),
          // mediaType, content, description are optional
        },
      }
      // console.log(JSON.stringify(node, undefined, 2))
      createNode(deckNode)
    }
  })
}
exports.onCreateNode = ({ node, getNode, actions }) => {
  // console.log(node.internal.type)
  // if (node.internal.type === "File") {
  //   console.log(JSON.stringify(node, undefined, 2))
  // }
}
// exports.onCreatePage123 = ({ page, actions }) => {
//   // const { createPage, deletePage } = actions
//   // const pageData = JSON.parse(
//   //   fs.readFileSync("./src/content/topic/example.json", { encoding: "utf-8" })
//   // )
//   // console.log(pageData)
//   // deletePage(page)
//   // // You can access the variable "house" in your page queries now
//   // createPage({
//   //   ...page,
//   //   context: {
//   //     ...page.context,
//   //     categories: pageData,
//   //   },
//   // })
// }

exports.onCreatePage = ({ page, actions }) => {
  const { createPage, deletePage } = actions
  if (page.path === "/") {
    deletePage(page)
    const semesterPath = path.resolve("src/pages/semester")
    let semesters = []
    fs.readdirSync(semesterPath).forEach(folder => {
      const folderPath = semesterPath + "/" + folder
      let semester = []
      fs.readdirSync(folderPath).forEach(fileName => {
        semester.push(fileName.split(".")[0])
      })
      semesters.push(semester)
    })
    const otherPath = path.resolve("src/pages/other")
    let others = []
    fs.readdirSync(otherPath).forEach(fileName => {
      others.push(fileName.split(".")[0])
    })
    createPage({
      ...page,
      context: {
        ...page.context,
        semesters: semesters,
        others: others,
      },
    })
  }
}

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, "src"), "node_modules"],
    }
  })
}
