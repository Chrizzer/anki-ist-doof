import React from "react"
import "./style.css"

export default class Definition extends React.Component {
  render() {
    return (
      <div className="def-container">
        <div className="def-header">
          <span>Definition ({this.props.title})</span>
        </div>
        <div className="def-body">{this.props.children}</div>
      </div>
    )
  }
}
