import "../../mdx.css"
import Layout from "components/default-page-layout.jsx"

import Image from "components/image/image.fluid"

export default ({ children }) => (
  <Layout id="mdx" headerName="Rechnerarchitektur">
    {children}
  </Layout>
)

## Grundlegende Vokabeln

### Rechenwerk

Das Rechenwerk führt gemäß den Steueranweisungen Sequenzen von Mikroinstruktionen aus, interpretiert also den einzelnen Maschinenbefehl bis zur vollständigen Ausführung. Die Anzahl der Rechenwerke wird mit $d$ notiert.

### Leitwerk

Das Leitwerk interpretiert ein gegebenes Maschinenprogramm Instruktion für Instruktion und steuert so die gesamten Abläufe im Rechner, indem das Programm interpretiert wird. Die Anzahl der Leitwerke wird mit $k$ notiert.

## ECS-Klassifikation

<Image imgName="ra-ecs.png" />

### ECS -> Flynn

<Image imgName="ra-ecs_flynn.png" />

## Parallelism

### Phasen der Befehlsverarbeitung

<Image imgName="ra-befehlsverarbeitung.png" />

### Sequentielle Verarbeitung

Befehle werden nacheinander abgearbeitet

<Image imgName="ra-seq.png" />

### Nebenläufige Verarbeitung

Befehle werden gleichzeitig von unterschiedlichen Rechenwerken abgearbeitet

<Image imgName="ra-concurrency.png" />

### Pipelining

Mehrere (verschiedene) Rechenwerke können gleichzeitig benutzt werden

<Image imgName="ra-pipelining.png" />

#### Speedup

$$
\begin{align\*}
\text{speedup} &= \frac{n*k}{n+k-1} \\\\
\text{wobei}& \\\\
n &= \text{Befehle}\\\\
k &= \text{Stufen}
\end{align\*}
$$

#### Operator Forwarding

Unterschied zwischen Operator/Result forwarding bei einem superskalaren Prozessor: Operand Fetch wird zur gleichen Zeit des Write Backs ausgeführt.

<Image imgName="ra-operator_forwarding.png" />

## Caches

### Voll-Assoziativer Cache

Ein Voll-assoziativer Cache kann jede 'Linie' aus dem Hauptspeicher aufnehmen.

**Hohe Trefferrate**, bei _hohen Aufwand_

<Image imgName="ra-cache-1.png" />

### Direkt Abgebildeter Cache

Ein direkt abgebildeter Cache kann durch Restklassen-Einteilung (modulo) nur einen Teil der Addressen aus dem Hauptspeicher aufnehmen.

**Leichter implementierbar** und **schnelle Zugriffe**, weil keine Suche erforderlich ist, aber _geringe Trefferrate_

<Image imgName="ra-cache-2.png" />

### Mengen-Assoziativer Cache

Ein mengen-assoziativer Cache bildet durch Restklassen-Einteilung (modulo) Teile der Addressen in bestimmte Mengen ab. (Teil-)Mengen müssen lediglich durchsucht werden.

**Guter Kompromiss von Trefferrate** und **Aufwand**

<Image imgName="ra-cache-3.png" />

### Zugriffszeit

$$
\begin{align\*}
T_s &= T_1 + (1-H) * T_2\\\\
\text{wobei}& \\\\
T_s &= \text{System-Zugriffszeit}\\\\
T_1 &= \text{Cache-Zugriffszeit}\\\\
T_2 &= \text{Hauptspeicher-Zugriffszeit} \\\\
H &= \text{Cache-Trefferate}
\end{align\*}
$$

### Ersetzungsstrategien

**Random** - zufällige Lines werden verdrängt.

**Least Recently Used** - Nach einem extra LRU Bit werden die am längsten nicht referenzierten Linien verdrängt.

### Rückspeicherungsstrategien

**Write-Through** - Wenn Linie verändert wird, wird Änderung sofort in Hauptspeicher geschrieben;

**Write-Back** - Wenn Linie verdrängt wird, wird Änderung in Hauptspeicher geschrieben.

## Datenabhängigkeiten

### Read-After-Write

Eine Anweisung greift lesend auf das Ergebnis einer vorangehen Anweisung zu.

$$
\begin{align}
R1 &= R2+R3 \\\\
R4 &= R1+1
\end{align}
$$

### Write-After-Read

Eine Anweisung (über-)schreibt ein Register, auf das zuvor lesend zugegriffen wurde.

$$
\begin{align}
R1 &= R2 + R3 \\\\
R2 &= R4 - 1$
\end{align}
$$

### Write-After-Write

Zwei Anweisungen (über-)schreiben das gleiche Register.

$$
\begin{align}
R1 &= R2 / R3 \\\\
R1 &= R1 * 1
\end{align}
$$

## Branch Prediction

### 1-Bit Sprungvorhersage Automat

> Eine Branch History Table protokolliert vorherige Sprünge und wird nach jedem Sprung aktualisiert.

Ein 1-Bit Automat berücksichtigt nur das letzte Outcome.

$$
\begin{align\*}
f(x) &= \begin{cases}
x = \text{Branch} &\rightarrow \text{Branch} \\\\
x = \text{No Branch} &\rightarrow \text{No Branch}
\end{cases} \\\\
\text{wobei}& \\\\
x &= \text{State (last outcome)}
\end{align\*}
$$

### 2-Bit Sprungvorhersage Automat

Ein N-Bit Automat berücksicht die letzten N Branches (oft 2).
So werden die Mispredictions reduziert und insgesamt ein besseres Ergebnis erzielt.

## Invalidierungsprotokolle

### Write-Through-Caches

Write-Through-Caches broadcasten jede Schreiboperation über den Bus.
Betroffene Linien können so direkt invalidiert werden und neu gelesen werden.

Nachteil ist ein hoher Busverkehr und höhere Belastung der Cache-Controller durch ständiges Mithören und Vergleichen.

### MESI-Protokoll

Das MESI-Protokoll beschreibt 4 Zustände einer Cache-Linie

- (M) Exclusive Modified - Die Linie befindet sich **exklusiv** im Cache und wurde **modifiziert**,
- (E) Exclusive Unmodified - Die Linie befindet sich **exklusiv** im Cache und wurde **nicht modifiziert**,
- (S) Shared Unmodified - Die Linie befindet sich in **mehreren** Caches und wurde **nicht modifiziert**,
- (I) Invalid - Die Linie ist **ungültig**

## Multithreading

Multithreading hat hauptsächlich mit Kontextwechsel zu tun.

> - **Fine-Grained**: Kontextwechsel zyklisch bei jedem Takt
> - **Coarse-Grained**: Kontextwechsel wenn der Thread warten muss (dauert paar Takte)
> - **Simultaneous**: Befehle werden dynamisch auf freie Funktionseinheiten verteilt (Scoreboard)

## Rechnernetze

### Topologie

> **Bisektionsweite**: Mindestanzahl von Kanten die entfernt werden müssen um Netz mit $N$ Knoten in 2 Subnetze mit jeweils $N/2$ Knoten zu teilen. <br/> > **Grad**: Anzahl von Links die von einem Netzwerk ausgehen

#### Bus

Busse dienen der Komminkation digitaler Subsysteme.

Eine **Bustranskation** wickelt einen vollständigen Datentransfer ab über mehrere Buszyklen. Sie können _lesend_ oder _schreibend_ sein.

Der **Bus-Master**, bestimmt durch (1..n) **Bus-Arbiter**, startet die Transaktion und **Bus-Slaves** reagieren.

#### Kreuzschienenverteiler

Jeder kann mit jedem kommunizieren.

#### Omega-Netzwerk

<Image imgName="ra-omega-n.png" />

#### Butterfly-Netzwerk

<Image imgName="ra-butterfly-n.png" />

##### Nachricht-Kodierung

Abzweigungen sind mit 1 kodiert.

> Beispiel: <br/>
> 101 $\rightarrow$ unten, oben, unten <br/>
> 000 $\rightarrow$ oben, oben, oben

### Schaltmethoden

#### Circuit Switching

Ein Pfad zwischen Quelle und Ziel wird aufgebaut und aufrecht erhalten.

#### Packet Switching

Daten werden in Pakete zerteilt und individuell zum Ziel transportiert und dort zusammengesetzt.

> **Deadlock**:
