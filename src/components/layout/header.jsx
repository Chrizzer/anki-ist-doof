import React from "react"
import "./header.css"
import Menu from "../menu/menu"
import Image from "components/image/image.fluid"
import { renderMathInBody } from "components/default-page-layout.jsx"

export default class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <Menu />
        <h1 onClick={() => renderMathInBody()}>{this.props.name}</h1>
        <div className="image">
          <Image alt="me" imgName="bitmoji.png" />
        </div>
      </div>
    )
  }
}
