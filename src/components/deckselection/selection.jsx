import React from "react"
import "./style.css"
import Deck from "../deck/deck"
import { useStaticQuery, graphql, Link } from "gatsby"

export default function Selection(props) {
  const data = useStaticQuery(query)
  return (
    <div className="browser">
      <div className="header">
        <h1>Selection</h1>
      </div>
      <div className="pool">
        {[
          ["ki-1", "Künstliche Intelligenz 1"],
          ["wahrscheinlichkeitstheorie", "Wahrscheinlichkeitstheorie"],
          ["datenbanken", "Datenbanken"],
          ["ra", "Rechnerarchitektur"]
        ].map(tuple => (
          <Link className="not-a-deck" to={tuple[0]} state={{}}>
            <span className="center">{tuple[1]}</span>
          </Link>
        ))}
        {data.allDeck.nodes.map((node, i) => (
          <Deck
            key={`${node.name}${i}`}
            name={node.name}
            number={node.cards.length}
          />
        ))}
      </div>
    </div>
  )
}
const query = graphql`
  query decks {
    allDeck {
      nodes {
        name
        cards {
          deck
        }
      }
    }
  }
`
