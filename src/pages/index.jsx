import React from "react"
import Main from "../components/layout/main"
import Layout from "../components/default-page-layout"
import "./style.css"
import { Helmet } from "react-helmet"
import { inspect } from "util"

export default class Homepage extends React.Component {
  render() {
    const { others, semesters } = this.props.pageContext
    return (
      <>
        <Helmet>
          <title>Anki ist doof</title>
        </Helmet>
        <Layout>
          <Main others={others || []} semesters={semesters || []} />
        </Layout>
      </>
    )
  }
}
