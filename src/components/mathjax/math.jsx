import React from "react"
import "./style.css"
import { Helmet } from "react-helmet"
import waitForGlobal from "utils/waitForGlobal"
import Config from "utils/mathjax.config.js"

export default class Latex extends React.Component {
  constructor(props) {
    super(props)
    this.nodeRef = React.createRef()
  }
  componentDidMount() {
    waitForGlobal("MathJax").then(() => {
      window.top.MathJax.Hub.Config(Config())
    })
    this.renderMathjax()
  }

  componentDidUpdate() {
    this.renderMathjax()
  }

  renderMathjax() {
    if (window.top.MathJax != null) {
      window.top.MathJax.Hub.Queue([
        "Typeset",
        window.top.MathJax.Hub,
        this.nodeRef.current,
      ])
    }
  }

  render() {
    return (
      <>
        <Helmet>
          <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_SVG"
            async
          />
        </Helmet>
        <div ref={this.nodeRef}>{this.props.children}</div>
      </>
    )
  }
}
