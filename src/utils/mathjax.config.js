export default function() {
  return {
    jax: ["input/TeX", "output/SVG"],
    extensions: ["tex2jax.js"],
    messageStyle: "none",
    SVG: {
      scale: 100,
      minScaleAdjust: 100,
      linebreaks: {
        automatic: false,
        width: "container",
      },
    },
    tex2jax: {
      inlineMath: [["$", "$"], ["\\(", "\\)"]],
      displayMath: [["$$", "$$"], ["\\[", "\\]"]],
      preview: "none",
      balanceBraces: true,
      processEscapes: true,
      processEnvironments: true,
      skipTags: ["script", "noscript", "style", "textarea", "pre"],
    },
    TeX: {
      equationNumbers: { autoNumber: "AMS" },
      extensions: ["AMSmath.js", "AMSsymbols.js"],
      Macros: {
        // Insert Makros here!
        //
        "\\b": ["{\\textbf #1}", 1],
      },
    },
  }
}
// {
//   root: "",
//   config: [],
//   styleSheets: [],
//   styles: { ".MathJax_Preview": { color: "#888" } },
//   jax: [],
//   extensions: [],
//   preJax: null,
//   postJax: null,
//   displayAlign: "center",
//   displayIndent: "0",
//   preRemoveClass: "MathJax_Preview",
//   showProcessingMessages: true,
//   messageStyle: "normal",
//   delayStartupUntil: "none",
//   skipStartupTypeset: false,
//   elements: [],
//   positionToHash: true,
//   showMathMenu: true,
//   showMathMenuMSIE: true,
//   menuSettings: {
//     zoom: "None",
//     CTRL: false,
//     ALT: false,
//     CMD: false,
//     Shift: false,
//     discoverable: false,
//     zscale: "200%",
//     renderer: null,
//     font: "Auto",
//     context: "MathJax",
//     locale: null,
//     mpContext: false,
//     mpMouse: false,
//     texHints: true,
//     FastPreview: null,
//     assistiveMML: null,
//     inTabOrder: true,
//     semantics: false,
//   },
//   errorSettings: {
//     message: ["[", ["MathProcessingError", "Math Processing Error"], "]"],
//     style: { color: "#CC0000", "font-style": "italic" },
//   },
//   ignoreMMLattributes: {},
// }
