import "../../mdx.css"
import {
  Layout,
  Image,
  Problem,
  Definition,
  Example,
  Proof,
  Container,
  Codeblock,
} from "components/index.js"

export default ({ children }) => (
  <Layout id="mdx" headerName="Theoretische Informatik">
    {children}
  </Layout>
)

# Einführung

## Wörter und Sprachen

<Definition title="Alphabet, Symbole">
  Ein Alphabet $\Sigma$ ist eine <b>nichtleere endliche</b> Menge. Elemente
  eines Alphabets heißen <i>Symbole</i>.
  <Example>
    {"$\\text{Binäralphabet} = \\cset{0,1}$"}
    {"$\\text{Dezimalalphabet} = \\cset{0,1,\\dots,9}$"}
  </Example>
</Definition>
<Definition title="Wort, String">
  Ein Wort $w$ ist eine <b>endliche</b> Folge von Symbolen über ein Alphabet
  $\Sigma$.
  <Example>
    {"$ \\Sigma = \\cset{0,1}$"}
    {"$ \\Sigma^0 = \\cset{\\lambda}$"}
    {"$ \\Sigma^2 = \\cset{00,01,10,11}$"}
    {"$ \\Sigma^+ = \\Sigma^1 \\cup \\Sigma^2 \\cup \\dots $"}
    {"$ \\Sigma^* = \\cset{\\lambda} \\cup \\Sigma^+ $"}
  </Example>
</Definition>
<Definition title="Leere Wort">
  Das leere Wort $\lambda$ enthält keine Symbole. Es ist Teil jedes beliebigen
  Alphabets.
  {"$$\\lambda \\neq \\emptyset \\neq \\ \\cset{\\lambda}$$"}
</Definition>

### Operatoren auf Wörtern

- _Länge_ ($|w|$)
- _Konkatenation_ ($xy$)
- _Potenzierung_ ($w^{n+1}=w^nw$)
- _Symbole Zählen_ ($#_\sigma(w)$)

<Definition title="Sprache">
  Eine Sprache $L$ über Alphabet $\Sigma$ ist eine beliebige Teilmenge $L
  \subseteq \Sigma^*$
  <Example>
    {"$A \\cup B = \\cset{w: w \\in A \\text{ oder } w \\in B}$"}
    {"$A \\cap B = \\cset{w: w \\in A \\text{ und } w \\in B}$"}
    {
      "$\\overline A = \\cset{w \\in \\Sigma^*: w \\notin A} = \\Sigma^* \\setminus A$"
    }
  </Example>
  <Example>
    {"$A (B \\cup C) = AB \\cup AC$"}
    {"$(A \\cup B)C = AC \\cup BC$"}
    {"$A^*A^* = A^*$"}
    {"$(A^*)^* = A^*$"}
    {"$AA^* \\cup \\cset{\\lambda} = A^*$"}
  </Example>
</Definition>

### Probleme

<Definition title="Entscheidungsprobleme">
  Ein Entscheidungsproblem $L$ ist eine Sprache über $\Sigma^*$ die nur{" "}
  <b>Instanzen</b> enthält
  {"$$\\text{w ist eine 'Ja'-Instanz gdw. }w \\in L$$"}
</Definition>

|          Art          | Beschreibung                                                              |
| :-------------------: | :------------------------------------------------------------------------ |
| Entscheidungsprobleme | Jede Probleminstanz wird einem Wahrheitswert zugewiesen, _Ja_ oder _Nein_ |
|  Berechnungsproblem   | Jede Probleminstanz wird auf einen diskreten Wert abgebildet              |

## Grammatiken

<Definition title="Grammatik">
  Eine Grammatik $G=(T,N,S,P)$ besteht aus einem <br />
  Terminal- und Nonterminalalphabet sind disjunkt.
  <ul>
    <li>Terminalalphabet $T$ (Alphabet)</li>
    <li>Nonterminalalphabet $N$ (Variablen)</li>
    <li>Startsymbol $S$</li>
    <li>Produktionsregeln $P$ ($l \implies r$)</li>
  </ul>
  <Example>
    {"$G=(T,N,S,P)$"}
    {"$T=\\cset{0,1}$"}
    {"$N=\\cset{S}$"}
    {`$P=\\cset{S \\implies \\lambda | 0 | 1 |0S0 |1S1}$`}
  </Example>
</Definition>

- Grammatiken definieren Sprachen
- Automaten akzeptieren oder verwerfen Wörter aus Sprachen
- Grammatiken heißen äquivalent wenn $L(G_1)=L(G_2)$

<Definition title="Ableitungsrelation $\stackrel{*}{\implies}$">
  Mit der Ableitungsrelation können Wörter aus einer Grammatik $G$ abgeleitet
  werden, so dass $y$ aus $x$ direkt <br />
  <div style="display: flex;">
    <div style="margin: auto;">
      {`$\\begin{aligned}
  \\text{1 Schritt} &: x \\implies y \\\\
  \\text{n Schritte} &: x \\stackrel{*}{\\implies} y
  \\end{aligned}$`}
    </div>
  </div>
</Definition>
<Definition title="Sprache $L(G)$">
  Die Sprache $L$ wird durch eine Grammatik und enthält alle Wörter die sich aus
  der Grammatik $G=(T,N,S,P)$ ableiten lassen.
  {`$$L(G) = \\cset{w \\in T^*|S \\stackrel{*}{\\implies} w}$$`}
</Definition>
<Definition title="Äquivalenz von Grammatiken">
  Zwei Grammatiken sind äquivalent wenn sie das gleiche Terminalalphabet
  benutzen und die erzeugten Sprachen $L(G_1)=L(G_2)$ sind.
  <Proof>
    {`Zu zeigen: $A=L(G)$, also $A \\subseteq L(G)$ und $L(G) \\subseteq A$`}
    <ol>
      <li>
        {`Angenommen $w \\in A$. Beweise, dass $w \\in L(G)$ also $S \\stackrel{*}{\\implies} w$`}
        <br />
        Beweis per <b>Induktion</b> über <b>Wortlänge</b> $|w|$
      </li>
      <li>
        {`Angenommen $w \\in L(G)$. Beweise, dass $w \\in A$`}
        <br />
        {
          "Beweis per $\\b{Induktion}$ über $\\b{Anzahl der Schritte}$ in Ableitung $S \\stackrel{*}{\\implies} w$"
        }
      </li>
    </ol>
  </Proof>
</Definition>
<Definition title="Reguläre Grammatik">
  Eine Grammatik $G$ heißt regulär, wenn alle Regeln $l \implies r$ in P
  folgende Eigenschaften haben:
  <ul>
    <li>$l$ ist einzelnes Nonterminal ($l \in N$)</li>
    <li>
      $r$ enthält höchstens ein Nonterminal am Ende ($r \in T^*\cup (T^*N)$)
    </li>
  </ul>
</Definition>
<Definition title="Kontextfreie Grammatik">
  Eine Grammatik $G$ heißt kontextfrei, wenn alle Regeln $l \implies r$ in P
  folgende Eigenschaften haben:
  <ul>
    <li>$l$ ist einzelnes Nonterminal ($l \in N$)</li>
  </ul>
</Definition>
<Definition title="Kontextsensitive Grammatik">
  Eine Grammatik $G$ heißt kontextsensitiv, wenn alle Regeln $l \implies r$ in P
  folgende Eigenschaften haben:
  <ul>
    <li>$l=uXv$ mit $X \in N$ und $u,v \in (N \cup T)^*$</li>
    <li>$r=uwv$ mit $w \in (N \cup T)^+$</li>
  </ul>
  Zusätzlich darf $G$ $\lambda$-Sonderregel enthalten, so dass
  <ul>
    <li>$(S \implies \lambda) \in P$ und</li>
    <li>In keiner Regel aus $P$ kommt $S$ in $r$ vor</li>
  </ul>
</Definition>

## Abgeschlossenheit von Sprachoperationen

<Proof>
  {`$$ \\underbrace{REG}_{\\text{Typ-3}} \\subsetneq \\underbrace{CFL}_{\\text{Typ-2}} \\subsetneq \\underbrace{CSL}_{\\text{Typ-1}} \\subsetneq \\underbrace{RE}_{\\text{Typ-0}}$$`}
</Proof>
<Definition title="Abgeschlossenheit">
  Eine Sprachklasse $C$ ist abgeschlossen gegenüber einer Operation $\circ$,
  wenn die Anwendung der Operation auf zwei Sprachen $A,B \in C$ nur Sprachen
  aus $C$ ergibt.
  {`$$A,B \\in C : A \\circ B \\in C$$`}
</Definition>
<Container>
  <table class="centered" style="overflow-x: scroll;">
    <tr>
      <th>Operation</th>
      <th>Regulär (REG)</th>
      <th>Kontextfrei (CFL)</th>
      <th>Kontextsensitiv (CSL)</th>
    </tr>
    <tr>
      <td>Vereinigung</td>
      <td>Ja</td>
      <td>Ja</td>
      <td>Ja</td>
    </tr>
    <tr>
      <td>Schnitt</td>
      <td>Ja</td>
      <td>Nein</td>
      <td>Ja</td>
    </tr>
    <tr>
      <td>Konkatenation</td>
      <td>Ja</td>
      <td>Ja</td>
      <td>Ja</td>
    </tr>
    <tr>
      <td>Komplement</td>
      <td>Ja</td>
      <td>Nein</td>
      <td>Ja</td>
    </tr>
    <tr>
      <td>Kleeneabschluss</td>
      <td>Ja</td>
      <td>Ja</td>
      <td>Ja</td>
    </tr>
  </table>
</Container>

# Automaten

## Deterministischer endlicher Automat DFA

<Definition title="Deterministischer endlicher Automat">
  Ein DFA $M=(Q,\Sigma,\delta,q_0,F)$ besteht aus
  <ul>
    <li>endliche Menge von Zuständen $Q$</li>
    <li>Eingabealphabet $\Sigma$</li>
    <li>Übergangsfunktion $\delta:Q\times \Sigma \implies Q$</li>
    <li>Startzustand $q_0 \in Q$</li>
    <li>akzeptierende / finale Zustände $F \subseteq Q$</li>
  </ul>
  Die Zustandsübergangsfunktion gibt an, in welchen Zustand der Automat übergeht
  wenn er sich in Zustand $q$ befindet und Symbol $s$ ließt : $\delta(q,s)$
  <br />
  {`$\\begin{aligned}
  \\text{M akzeptiert $w \\in \\Sigma^*$} &: \\hat{\\delta}(q_0,w) \\in F \\\\
  \\text{M verwirft $w \\in \\Sigma^*$} &: \\hat{\\delta}(q_0,w) \\notin F
  \\end{aligned}$`}
  <Image imgName="dfa-graph.png" />
  <Image imgName="dfa-table.png" />
</Definition>

## Produktautomat

<Definition title="Produktautomat">
  Der Produktautomat $M$ setzt sich für zwei feste DFAs $M_1,M_2$ so zusammen:
  <ul>
    <li>$Q=Q_1\times Q_2=\lbrace (p \in Q_1,q \in Q_2) \rbrace$</li>
    <li>$F=F_1\times F_2=\lbrace (p \in F_1,q \in F_2) \rbrace$</li>
    <li>$q_0 = (q_1,q_2)$</li>
    <li>$\delta((p,q),s)=(\delta_1(p,s),\delta_2(q,s))$</li>
  </ul>
  Der Produktautomat akzeptiert genau den Schnitt der beiden Sprachen $L(M) =
  L(M_1) \cap L(M_2)$
</Definition>

## Nichtdeterministischer endlicher Automat NFA

<Definition title="Nichtdeterministischer endlicher Automat">
  Ein NFA $N=(Q,\Sigma,\Delta,q_0,F)$ besteht aus
  <ul>
    <li>endliche Menge von Zuständen $Q$</li>
    <li>Eingabealphabet $\Sigma$</li>
    <li>Übergangsfunktion $\Delta:Q\times \Sigma \implies 2^Q$(Potenzmenge)</li>
    <li>Startzustand $q_0 \in Q$</li>
    <li>akzeptierende / finale Zustände $F \subseteq Q$</li>
  </ul>
  Die Zustandsübergangsfunktion gibt an, welche Zustände erreicht werden können.
  Es kann sein, dass kein Zustand erreicht wird, dann ist
  $\Delta(q,s)=\emptyset$ <br />
  {`$\\begin{aligned}
  \\text{N akzeptiert $w \\in \\Sigma^*$} &: \\hat{\\Delta}(\\cset{q_0},w) \\cap F \\neq \\emptyset \\\\
  \\text{N verwirft $w \\in \\Sigma^*$} &: \\hat{\\Delta}(\\cset{q_0},w) \\cap F = \\emptyset
  \\end{aligned}$`}
</Definition>
<Definition title="Guess-And-Verify">
  Mit Guess-And-Verify werden zunächst nichtdeterministisch alle Möglichkeiten
  getestet und dann verifiziert ob ein akzeptierender Zustand erreicht wurde.
</Definition>
<Proof>
  {`Jeder DFA $M$ ist äuqivalent zu einem NFA $N$, so dass $L(M)=L(N)$.`}
  {`Zu jedem NFA gibt es einen äquivalenten DFA.`}
  <b>{`Potenzmengenkonstruktion`}</b>
</Proof>
<Definition title="Potenzmengenkonstruktion">
  Für einen NFA $N=(Q,\Sigma,\Delta,q,F)$ wird ein äquivalenter DFA
  $M=(P,\Sigma,\delta,p,F')$ so definiert, dass:
  <ul>
    <li>$P=2^Q$ (Alle Kombinationen von Zuständen)</li>
    <li>$\delta(A,s)=\hat \Delta (A,s)$ (Kombination $A$ von Zuständen)</li>
    <li>$p = \cset q$ (Startzustand)</li>
    <li>
      $F' = \lbrace A \subseteq Q: A \cap F \neq \emptyset \rbrace$ (Alle
      Kombinationen mit akz. Zustand)
    </li>
  </ul>
  Achtung, die Funktion $\delta(\emptyset,s) = \emptyset$ also aus keinem
  Zustand hält der Automat.
</Definition>
<Definition title="Lambda-Übergang">
  Ein $\lambda$-Übergang in einem NFA kann benutzt werden ohne ein Symbol
  gelesen zu haben. <br />
  Die $\lambda$-Closure für einen Zustand $q$ beschreibt alle über $\lambda$-Übergänge
  erreichbaren Knoten von $q$. Mit Hilfe der Closure kann mit der Potenzmengenkonstruktion
  für einen $\lambda$-NFA ein DFA konstruiert werden.
</Definition>

## Konstruktion einer regulären Grammatik aus DFA

<Proof>
  <span>{`Sei ein DFA $M=(Q,\\Sigma,\\delta,q_0,F)$ für eine reg. Grammatik G`}</span>
  <ul>
    <li>$N=Q$</li>
    <li>$S=q_0$</li>
    <li>$q_0 \implies \lambda$, wenn $q_0 \in F$</li>
    <li>$q \implies sp$, wenn $\delta(q,s)=p$</li>
    <li>$q \implies s$, wenn $\delta(q,s) \in F$</li>
  </ul>
</Proof>

## Konstruktion eines $\lambda$-NFA aus reg. Grammatik

<Proof>
  <span>
    Produktionsregeln ($l \implies r$) umformen, sodass folgende Eigenschaften
    erfüllt werden:
  </span>
  <ul>
    <li>$l \in N$</li>
    <li>$r \in \lbrace \lambda \rbrace \cup \Sigma \cup N \cup (\Sigma N)$</li>
  </ul>
  <Example>
    {`$$\\begin{aligned}
    X &\\implies s_0,s_1,\\dots,s_nY \\text{ wird ersetzt durch} \\\\
    X &\\implies s_0Y_0 \\\\
    Y_0 &\\implies s_1Y_1 \\\\
    Y_i &\\implies s_iY_{i+1} \\\\
    Y_{n-1} &\\implies s_nY
    \\end{aligned}$$`}
    {`$$\\begin{aligned}
    X &\\implies s_0,s_1,\\dots,s_m \\text{ wird ersetzt durch} \\\\
    X &\\implies s_0X_0 \\\\
    X_0 &\\implies s_1X_1 \\\\
    X_i &\\implies s_iX_{i+1} \\\\
    X_{m-1} &\\implies s_m
    \\end{aligned}$$`}
  </Example>
  <span>Ein $\lambda$-NFA $N$ wird wie folgt konstruiert:</span>
  <ul>
    <li>$Q= N \cup \lbrace q_F \rbrace$, wobei $q_F \notin N$</li>
    <li>$q_0 = S$</li>
    <li>$F = \lbrace q_F \rbrace$</li>
    <li>
      $X,Y \in N$ und $s \in \Sigma$
      <ul>
        <li>$Y \in \Delta(X,s)$, wenn $X \implies sY$</li>
        <li>$Y \in \Delta(X,\lambda)$, wenn $X \implies Y$</li>
        <li>$q_F \in \Delta(X,s)$, wenn $X \implies s$</li>
        <li>$q_F \in \Delta(X,\lambda)$, wenn $X \implies \lambda$</li>
      </ul>
    </li>
  </ul>
</Proof>

## Grenzen endlicher Automaten

### Pumping-Lemma für reguläre Sprachen

<Definition title="Pumping-Lemma für reguläre Sprachen">
  Sei $L$ eine reguläre Sprache. Dann
  <ul>
    <li>existiert ein $n \geq 0$, so dass</li>
    <li>für alle Worte $w \in L$ mit $|w| \geq n$ gilt, dass</li>
    <li>
      eine Zerlegung $w=xyz$ existiert mit $|y| \geq 1$ und $|xy| \leq n$, so
      dass
    </li>
    <li>$xy^kz \in L$ für alle $k \geq 0$ gilt</li>
  </ul>
</Definition>
<Proof>
  <span>{`$L$ ist regulär $\\implies$ $P$ (Pumping-Eigenschaft)`}</span>
  <span>{`$\\neg P$ $\\implies$ $L$ ist nicht regulär`}</span>
</Proof>
<Definition title="Pumping-Lemma für reg. Sprachen in Kontraposition">
  Sei $L$ eine Sprache, so dass
  <ul>
    <li>für alle $n \geq 0$</li>
    <li>ein Wort $w \in L$ existiert mit $|w| \geq n$, so dass</li>
    <li>für alle Zerlegungen $w=xyz$ mit $|y| \geq 1$ und $|xy| \leq n$</li>
    <li>ein $k \geq 0$ existiert mit $xy^kz \notin L$</li>
  </ul>
  Dann ist $L$ <b>nicht</b> regulär.
</Definition>

## Reguläre Ausdrücke

### Automat für M1 oder M2 $M=(M1+M2)$

<Image imgName="a+b_automat.jpg" />

### Automat für M1 gefolgt von M2 $M=M1M2$

<Image imgName="ab_automat.jpg" />

### Automat für mehrere M1 $M=M1^*$

<Image imgName="a_automat.jpg" />

## Satz von Myhill-Nerode

<Definition title="Äquivalenzrelation $\equiv_L$">
  Für eine Sprache $L \subseteq \Sigma^*$ ist die Äquivalenzrelation wie folgt
  definiert: $$ x \equiv_L y \iff \forall z \in \Sigma^*(xz \in L \iff yz \in L)
  $$
</Definition>
<Definition title="Nerode-Klassen">
  Für eine Sprache $L \subseteq \Sigma^*$ ist die Nerode-Klasse /
  Äquivalenzklasse eines Wortes $x \in \Sigma^*$ $N_L(x)$ die Menge aller Worte
  $y$ die äquivalent bezüglich $\equiv_L$ zu $x$ sind: $$ N_L(x) = \lbrace y \in
  \Sigma^*:y \equiv_L x \rbrace $$
</Definition>
<Definition title="Satz von Myhill-Nerode">
  Eine Sprache $L$ ist genau dann regulär, wenn sie einen endlichen Index hat,
  also endliche viele Nerode-Klassen hat.
</Definition>

# Kontextfreie Sprachen

<Definition title="Chomsky-Normalform">
  Eine Grammatik $G=(N,T,S,P)$ ist in Chomsky-Normalform, wenn alle Regeln einer
  dieser Formen entsprechen:
  <ul>
    <li>$A \to BC$ mit $A,B,C \in N$ oder</li>
    <li>$A \to a$ mit $A \in N$ und $a \in T$</li>
    <li>$S \to \lambda$ (falls $G$ auch leeres Wort akz.)</li>
  </ul>
</Definition>
<Proof>
  Für jede kontextfreie Grammatik existiert eine äquivalente Grammatik in
  Chomsky-Normalform.
</Proof>
<Proof>
  <b>Umwandlung einer Grammatik in Chomsky-NF</b>
  {`Grammatik $G \\vdash G'$ in Chomsky-NF`}
  {`(1) $\\lambda$-Produktion = $A \\in N \\to \\lambda$`}
  {`(2) Einheits-Produktion = $A \\in N \\to B \\in N$`}
  <ol>
    <li>$\lambda$-Produktionen und Einheits-Produktionen entfernen</li>
    <li>Regeln der Form $A \to w$ mit $|w| \geq 2$ umwandeln</li>
    <li>$\lambda$-Sonderregel wieder einführen, falls $\lambda \in L(G)$</li>
  </ol>
</Proof>
<Definition title="Pumping-Lemma für kontextfreie Sprachen">
  Sei $L$ eine kontextfreie Sprache. Dann
  <ul>
    <li>existiert ein $n \geq 0$, so dass</li>
    <li>für alle Worte $z \in L$ mit $|z| \geq n$ gilt, dass</li>
    <li>
      eine Zerlegung $w=uwvxy$ existiert mit $|vx| \geq 1$ und $|vwx| \leq n$,
      so dass
    </li>
    <li>für alle $k \geq 0$ $uv^kwx^ky \in L$ gilt</li>
  </ul>
</Definition>
<Definition title="Pumping-Lemma für kontextfreie Sprachen in Kontraposition">
  Sei $L$ eine Sprache, so dass
  <ul>
    <li>für alle $n \geq 0$</li>
    <li>ein Wort $z \in L$ existiert mit $|z| \geq n$, so dass</li>
    <li>für alle Zerlegungen $w=uvwxy$ mit $|vx| \geq 1$ und $|vwx| \leq n$</li>
    <li>ein $k \geq 0$ existiert mit $uv^kwx^ky \notin L$</li>
  </ul>
  Dann ist $L$ <b>nicht</b> regulär.
</Definition>

## Pushdown-Automaten

<Problem
  name="Wortproblem"
  input="Ein Wort $w \in \Sigma^*$"
  question="Entscheide, ob $w \in L$?"
/>
<Definition title="Nichtdeterministischer Pushdown-Automat">
  Ein (N)PDA ist ein Tupel $M=(Q,\Sigma,\Gamma,\delta,q_0,\bot,F)$, wobei
  <ul>
    <li>endliche Zustandsmenge $Q$</li>
    <li>Eingabealphabet $\Sigma$</li>
    <li>Arbeitsalphabet $\Gamma$</li>
    <li>Zustandsübergangsrelation $\delta$</li>
    <li>Startzustand $q_0 \in Q$</li>
    <li>Pushdown-Startsymbol $\bot \in \Gamma$ (wenn Stack leer)</li>
    <li>akzeptierende Zustandsmenge $F$</li>
  </ul>
  Ein PDA akzeptiert ein Wort, wenn Wort komplett gelesen wurde und
  <ul>
    <li>
      Akzeptierender Zustand erreicht und Stack egal <b>oder</b> <br />
      {`$(q_0,x,\\bot) \\stackrel{*}{\\vdash} (q_F,\\lambda,\\varphi)$`}
    </li>
    <li>
      Zustand egal und Stack leer
      <br />
      {`$(q_0,x,\\bot) \\stackrel{*}{\\vdash} (q,\\lambda,\\lambda)$`}
    </li>
  </ul>
</Definition>
<Definition title="Konfiguration PDA">
  Eine Konfiguration für einen PDA $M=(Q,\Sigma,\Gamma,\delta,q_0,\bot,F)$ ist
  {`$$(q, y, \\beta)$$`}
  wobei
  <ul>
    <li>$q$ ist der aktuelle Zustand</li>
    <li>$y$ ist das noch zu lesende Wort</li>
    <li>$\beta$ ist der Stack (Oben, Unten)</li>
  </ul>
  <Example>{`Anfangskonfiguration für Eingabewort x: $(q_0,x,\\bot)$`}</Example>
</Definition>
<Definition title="Konfigurationswechsel PDA">
  {`$$ \\delta \\subseteq (Q \\times (\\Sigma \\cup \\cset{\\lambda}) \\times \\Gamma) \\times (Q \\times \\Gamma^*))$$`}
  {`$$(\\textcolor{blue}{(p,a,A)},\\textcolor{red}{(q,B_1,B_2,\\dots,B_k)}) \\in \\delta $$`}
  <ul>
    <li>{`$\\textcolor{blue}{p}$`} aktueller Zustand</li>
    <li>{`$\\textcolor{blue}{a}$`} gelesenes Eingabesymbol</li>
    <li>{`$\\textcolor{blue}{A}$`} Top of Stack</li>
    <li>{`$\\textcolor{red}{q}$`} nächster Zustand </li>
    <li>{`$\\textcolor{red}{B_1,B_2,\\dots,B_k}$`} neuer Top of Stack</li>
  </ul>
  <Example>
    <span>
      Zustand q nach p, Kopf ließt x, Stack ist $T$ und push($\varphi$)
    </span>
    {`$(q,ab,T) \\vdash (p,b,\\varphi T)$`}
    <span>Konfigurationswechsel für $((p,\lambda,A),(q,\varphi))$</span>
    <span>
      <i>Übergang erfolgt, ohne das Eingabewort weiterzulesen</i>
    </span>
    {`$(q,\\lambda,A\\beta) \\vdash (p,s,\\varphi\\beta)$`}
  </Example>
</Definition>

## Deterministische Pushdown-Automaten

<Definition title="Deterministischer Pushdown-Automat">
  Ein (D)PDA ist ein Tupel $M=(Q,\Sigma,\Gamma,\delta,q_0,\bot, \lhd,F)$, wobei
  <ul>
    <li>
      $\lhd \notin \Sigma$ rechter Endmarker
      {`$$ \\delta \\subseteq (Q \\times (\\Sigma \\cup \\cset{\\lambda, \\lhd}) \\times \\Gamma) \\times (Q \\times \\Gamma^*))$$`}
    </li>
    <li>
      deterministisch bedeutet, dass genau eine Transition für alle Zustände,
      Symbole, Arbeitssymbole existiert in der Form:
      {`$$((p,a,A),(q,T)) \\text{ oder } ((p,\\lambda,A),(q,T))$$`}
    </li>
    <li>
      alle Transitionen mit $\bot$ in der Form:
      {`$$((p,a,\\bot),(q,T\\bot))$$`}
    </li>
  </ul>
  <Example>{`Anfangskonfiguration für Eingabewort x: $(q_0,x\\lhd,\\bot)$`}</Example>
</Definition>

## CYK-Algorithmus

<Proof>
  <span>
    Der CYK-Algorithmus löst das Wortproblem für eine kontextfreie Grammatik
    $G=(N,T,S,P)$ in Chomsky-NF in $O(n^3)$. Dabei benutzt er dynamische
    Programmierung.
  </span>
  {`$$w \\in L(G) \\iff S \\stackrel{*}{\\implies} w \\iff S \\in N_{0,n}$$`}
  <Image imgName="cyk.png" /> <br />
  {`In jeder Zelle $N_{i,j}$ befindet sich die Menge die das Teilwort $w_i\\dots w_j$ ableiten kann.`}
  <Example>
    {`Das Teilwort $[\\,[\\,]\\,]$ an Stelle $N_{3,6}=S$ schaut es eine Regel für die Zerlegung $[-[\\,]\\,]$ oder $[\\,[\\,]-]$ gibt.`}
    {`$A \\implies [$ und $C \\implies [\\,]\\,]$ wobei`}
    {`$[\\,[\\,]$ lässt sich nicht ableiten und $B \\implies ]$`}
    {`Somit wird das Nonterminal gewählt, dass $AC$ ableitet $S$`}
  </Example>
</Proof>

# Entscheidbarkeit

## Turing Maschine

<Definition title="Deterministische Turing-Maschine">
  Eine DTM $M=(Q,\Sigma,\Gamma,\delta,q_0,F)$ besteht aus
  <ul>
    <li>$Q$ endliche Menge von Zuständen</li>
    <li>$\Sigma$ Eingabealphabet</li>
    <li>
      $\Gamma$ Arbeitsalphabet (<i>Alles was auf dem Band stehen kann</i>)
      <ul>
        <li>Blanksymbol $\sqcup \in \Gamma$</li>
        <li>$\Sigma \subseteq \Gamma$</li>
      </ul>
    </li>
    <li>
      $\delta$ partielle Übergangsfunktion
      {`$$\\delta: Q \\times \\Gamma \\to Q \\times \\Gamma \\times \\underset{\\text{links, rechts, neutral}}{\\cset{l,r,n}}$$`}
    </li>
    <li>$q_0 \subseteq Q$ Startzustand</li>
    <li>$F \subseteq Q$ akzeptierende Zustände</li>
  </ul>
</Definition>
<Definition title="Konfiguration PDA">
  Eine Konfiguration für eine DTM $M=(Q,\Sigma,\Gamma,\delta,q_0,F)$ ist
  {`$$(\\alpha, q, \\beta)$$`}
  wobei
  <ul>
    <li>$q$: der aktuelle Zustand</li>
    <li>$\alpha$: Inhalt links vom Kopf</li>
    <li>$\beta$: Inhalt Kopf + rechts</li>
  </ul>
  <Example>
    Für Eingabewort $x$
    <ul>
      <li>{`Anfangskonfiguration: $(\\lambda,q_0,x)$`}</li>
      <li>{`Endkonfiguration: $(x,q_F,\\sqcup)$`}</li>
    </ul>
  </Example>
</Definition>

<Definition title='Konfigurationswechsel'>

</Definition>

## Zeit- und Platzkomplexität

## Unentscheidbare Probleme

## Reduktionsmethode

## Der Satz von Rice

## Postsche Korrespondenzproblem

## Unentscheidbare Probleme bei formalen Sprachen

# Komplexitätstheorie

## Einführung

## Nichtdeterministische Komplexität

## Polynomielle Reduktion

## Der Satz von Cook

## NP-Vollständige Probleme

## Komplexitätsklassen

## Weitere Relationen zwischen Komplexitätsklassen

# Sprachen & Probleme

<Proof>
  {`$$PAREN=L(G=(\\cset{S},\\cset{[,]},S,\\cset{S \\to [S] \\mid SS \\mid \\lambda}))$$`}
</Proof>
