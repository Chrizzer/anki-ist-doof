import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const Image = ({ imgName }) => (
  <StaticQuery
    query={graphql`
      query {
        allImageSharp {
          edges {
            node {
              fluid(quality: 100) {
                originalName
                src
                srcSet
                aspectRatio
                sizes
                base64
                presentationWidth
                presentationHeight
              }
              resolutions {
                width
                height
              }
            }
          }
        }
      }
    `}
    render={data => {
      const image = data.allImageSharp.edges.find(
        edge => edge.node.fluid.originalName === imgName
      )
      if (!image) {
        return <div>Can't find Image</div>
      }
      let style = {
        // minHeight: image.node.resolutions.height,
        // minWidth: image.node.resolutions.width,
        // maxHeight: image.node.fluid.presentationHeight,
        maxWidth: image.node.fluid.presentationWidth,
      }
      return <Img style={style} draggable={true} fluid={image.node.fluid} />
    }}
  />
)
export default Image
