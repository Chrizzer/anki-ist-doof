import React, { createContext } from "react"
import "./style.css"
import { inspect } from "util"
import { renderMathInBody } from "components/default-page-layout.jsx"

let ShowContext = createContext(true)

export default class Problem extends React.Component {
  state = {
    showNotes: true,
  }

  static Name = ({ children }) => {
    return <div className="problem-name">{children}</div>
  }

  static Input = ({ children }) => {
    return (
      <div className="problem-input">
        <b>Eingabe: </b>
        <div>{children}</div>
      </div>
    )
  }

  static Question = ({ children }) => {
    return (
      <div className="problem-question">
        <b>Problem: </b>
        <div>{children}</div>
      </div>
    )
  }

  static Notes = ({ children }) => {
    return (
      <ShowContext.Consumer>
        {value =>
          value ? (
            <div className="problem-notes">
              <b>Notizen: </b>
              <div>{children}</div>
            </div>
          ) : null
        }
      </ShowContext.Consumer>
    )
  }

  render() {
    return (
      <div
        className="problem-container"
        /* onClick={() => {
          this.setState({ showNotes: !this.state.showNotes }, () => {
            renderMathInBody()
          })
        }} */
      >
        <ShowContext.Provider value={this.state.showNotes}>
          {this.props.children}
        </ShowContext.Provider>
      </div>
    )
  }
}
