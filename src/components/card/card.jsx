import React from "react"
import { Helmet } from "react-helmet"
import waitForGlobal from "./../../utils/waitForGlobal"
import "./style.css"
import Config from "./../../utils/mathjax.config.js"

export default class Card extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      question: this.props.question || "Undefined question",
      answer: this.props.answer || "Undefined answer",
      topic: this.props.topic || "Undefined topic",
      img: this.props.img || "Undefined img",
      snippet: this.props.snippet || "Undefined snippet",
    }
  }

  componentDidMount() {
    waitForGlobal("MathJax")
      .then(() => {
        window.top.MathJax.Hub.Config(Config())
      })
      .then(() => {
        this.render()
      })
    if (window.top.MathJax != null) {
      window.top.MathJax.Hub.Queue(["Typeset", window.top.MathJax.Hub])
    }
  }

  componentDidUpdate() {
    if (window.top.MathJax != null) {
      window.top.MathJax.Hub.Queue(["Typeset", window.top.MathJax.Hub])
    }
  }

  render() {
    const style = {
      display: !this.props.showAnswer ? "none" : "flex",
    }

    return (
      <>
        <Helmet>
          <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_SVG"
            async
          />
        </Helmet>
        <div className="card">
          <span className="center question">
            {this.state.question.split("\n").map((line, i) => (
              <span key={`${line}${i}`}>{line}</span>
            ))}
          </span>
          <div className="answer" style={style}>
            <span className="center">{this.state.answer}</span>
          </div>
        </div>
      </>
    )
  }
}
