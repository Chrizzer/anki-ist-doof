import Proof from "./proof/proof"
import Layout, { Container } from "components/default-page-layout.jsx"
import Image from "components/image/image.fluid"
import Problem from "components/problem/problem.jsx"
import Definition from "components/definition/definition.jsx"
import Example from "components/examples/example.jsx"
import Codeblock from "components/code/CodeBlock.jsx"
import { Link } from "gatsby"

export {
  Proof,
  Layout,
  Image,
  Problem,
  Definition,
  Example,
  Container,
  Codeblock,
  Link
}
