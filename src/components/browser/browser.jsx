import React from "react"
import "./style.css"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons"
import Card from "../card/card_parsed"

export default class Browser extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showAnswer: false,
      currentCard: 0,
    }
    this.next = this.next.bind(this)
    this.prev = this.prev.bind(this)
    this.toggleAnswer = this.toggleAnswer.bind(this)
  }

  toggleAnswer() {
    this.setState({
      showAnswer: !this.state.showAnswer,
    })
  }

  next() {
    this.setState({
      currentCard:
        this.props.pageContext.cards.length > this.state.currentCard + 1
          ? this.state.currentCard + 1
          : 0,
      showAnswer: false,
    })
  }

  prev() {
    this.setState({
      currentCard:
        0 <= this.state.currentCard - 1
          ? this.state.currentCard - 1
          : this.props.pageContext.cards.length - 1,
      showAnswer: false,
    })
  }

  render() {
    const card = this.props.pageContext.cards[this.state.currentCard]

    return (
      <div className="browser">
        <div className="header">
          <h1>{`${this.props.pageContext.name}#${this.state.currentCard +
            1}`}</h1>
        </div>
        <Card
          {...card}
          key={"card" + this.state.currentCard}
          showAnswer={this.state.showAnswer}
        />
        <div className="menu unselectable">
          <div onClick={() => this.prev()}>
            <FontAwesomeIcon icon={faArrowLeft} />
          </div>
          <div className="shower" onClick={() => this.toggleAnswer()}>
            <span>
              {!card.answer
                ? ""
                : "Show " + (this.state.showAnswer ? "Question" : "Answer")}
            </span>
          </div>
          <div onClick={() => this.next()}>
            <FontAwesomeIcon icon={faArrowRight} />
          </div>
        </div>
      </div>
    )
  }
}
