import React from "react"
import { separateChildrenProps } from "../examples/example"
import "./style.css"

export default function Proof(props) {
  let children = separateChildrenProps(props.children)
  if (children.length <= 1) {
    children = props.children
  }
  return <div className="proof-container paper">{children}</div>
}
