import Codeblock from "components/code/CodeBlock.jsx"
import { MDXProvider } from "@mdx-js/react"
import React from "react"
import { Helmet } from "react-helmet"
import "./style.css"
import Header from "components/layout/header"
import Footer from "components/layout/footer"
import renderMathInElement from "katex/dist/contrib/auto-render"
import "katex/dist/katex.min.css"
import { customMacros } from "./macros.js"
import CodeBlock from "./code/CodeBlock"

function preToCodeBlock(preProps) {
  if (
    // children is MDXTag
    preProps.children &&
    // MDXTag props
    preProps.children.props &&
    // if MDXTag is going to render a <code>
    preProps.children.props.name === "code"
  ) {
    // we have a <pre><code> situation
    const {
      children: codeString,
      props: { className, ...props },
    } = preProps.children.props

    return {
      codeString: codeString.trim(),
      language: className && className.split("-")[1],
      ...props,
    }
  }
  return undefined
}

const components = {
  pre: props => <div {...props} />,
  code: CodeBlock,
}

export function renderMathInBody() {
  renderMathInElement(document.body, {
    ignoredTags: ["script", "noscript", "style", "code"],
    delimiters: [
      { left: "$$", right: "$$", display: true },
      { left: "\\[", right: "\\]", display: true },
      { left: "$", right: "$", display: false },
    ],
    macros: customMacros,
  })
}

export default class Layout extends React.Component {
  componentDidMount() {
    renderMathInBody()
    if (window) {
      if (window["__whmEventSourceWrapper"]) {
        if (window["__whmEventSourceWrapper"]["/__webpack_hmr"]) {
          if (
            window["__whmEventSourceWrapper"]["/__webpack_hmr"]
              .addMessageListener
          ) {
            window["__whmEventSourceWrapper"][
              "/__webpack_hmr"
            ].addMessageListener(event => {
              if (event.data) {
                try {
                  const json = JSON.parse(event.data)
                  // console.log(json.action)
                  if (json.action === "built") {
                    renderMathInBody()
                  }
                } catch (e) {}
              }
            })
          }
        }
      }
    }
  }
  render() {
    // console.log(JSON.stringify(this.props, null, 2))
    return (
      <>
        <Helmet>
          <title>Anki ist doof</title>
        </Helmet>
        <MDXProvider components={components}>
          <div className="grid">
            <Header name={this.props.headerName || "Anki ist doof"} />
            <div id={this.props.id} className="main">
              {this.props.children}
            </div>
            <Footer />
          </div>
        </MDXProvider>
      </>
    )
  }
}

export function Container(props) {
  return <div>{props.children}</div>
}
