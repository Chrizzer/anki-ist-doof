import React from "react"
import "./footer.css"

export default class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <span>How to reach me:</span>
        <a href="https://gitlab.com/Chrizzer">
          <img
            alt="gitlab icon"
            src="https://img.icons8.com/color/48/000000/gitlab.png"
          />
        </a>
        <a href="https://www.xing.com/profile/Christopher_Starck">
          <img
            alt="xing icon"
            src="https://img.icons8.com/color/48/000000/xing.png"
          />
        </a>
        <a href="https://www.linkedin.com/in/christopherstarck/">
          <img
            alt="linkedin icon"
            src="https://img.icons8.com/color/48/000000/linkedin.png"
          />
        </a>
        <a href="mailto:christopher.starck@googlemail.com">
          <img
            alt="gmail icon"
            src="https://img.icons8.com/color/48/000000/gmail.png"
          />
        </a>
      </div>
    )
  }
}
