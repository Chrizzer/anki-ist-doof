import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faBars, faTimes, faHome } from "@fortawesome/free-solid-svg-icons"
import "./style.css"
import { Link } from "gatsby"

export default function HomeButton(props) {
  return (
    <Link to="/" className="menu">
      <FontAwesomeIcon icon={faHome} size="lg" />
    </Link>
  )
}
class Menu2 extends React.Component {
  state = {
    open: false,
  }
  render() {
    return (
      <>
        <div
          className="menu"
          onClick={() => this.setState({ open: !this.state.open })}
        >
          <FontAwesomeIcon
            icon={this.state.open ? faTimes : faBars}
            size="lg"
          />
        </div>
        <div className={`actualMenu ${this.state.open ? "open" : ""}`}>
          {["MenuItem", "MenuItem", "MenuItem"].map((v, i) => (
            <div key={i} className="menuItem">
              <span style={{ margin: "auto" }}>{v}</span>
            </div>
          ))}
        </div>
      </>
    )
  }
}
