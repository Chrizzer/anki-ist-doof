import React from "react"
import { Helmet } from "react-helmet"
import waitForGlobal from "../../utils/waitForGlobal"
import "./style.css"
import Config from "../../utils/mathjax.config.js"
import parse from "../../utils/parser.jsx"

export default class Card extends React.Component {
  componentDidMount() {
    waitForGlobal("MathJax")
      .then(() => {
        window.top.MathJax.Hub.Config(Config())
      })
    if (window.top.MathJax != null) {
      window.top.MathJax.Hub.Queue(["Typeset", window.top.MathJax.Hub])
    }
  }

  componentDidUpdate() {
    if (window.top.MathJax != null) {
      window.top.MathJax.Hub.Queue(["Typeset", window.top.MathJax.Hub])
    }
  }

  render() {
    // console.log(JSON.stringify(this.props, null, 2))
    return (
      <>
        <Helmet>
          <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS-MML_SVG"
            async
          />
        </Helmet>
        {parse(this.props)}
      </>
    )
  }
}
