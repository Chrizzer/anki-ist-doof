import "../../mdx.css"
import {
  Layout,
  Image,
  Problem,
  Definition,
  Example,
  Proof,
  Container,
  Codeblock,
  Link,
} from "components/index.js"

export default ({ children }) => (
  <Layout id="mdx" headerName="Algorithmen Design">
    {children}
  </Layout>
)

# Einleitung

## Algorithmisches Problemlösen

<Proof>
  <ol>
    <li>Problem verstehen</li>
    <li>
      Problem <b>modellieren und spezifizieren</b>
    </li>
    <li>Überprüfen, ob Problem algorithmisch lösbar ist</li>
    <li>
      geeignete
      <b>Lösungsstrategien, algorithmische Methoden und Datenstrukturen</b>
      auswählen oder entwickeln
    </li>
    <li>
      Algorithmus als <b>hochsprachliches Programm</b> beschreiben
    </li>
    <li>
      <b>Korrektheitsbeweis</b> führen (Zulässigkeit, Optimalität der Lösungen)
    </li>
    <li>
      <b>Effizienzanalyse</b>: Laufzeit und Speicherbedarf in Abhängigkeit der
      Problemgröße
    </li>
    <li>
      <b>Komplexitätsanalyse</b> des Problems: untere Schranken für die
      erforderlichen algorithmischen Ressourcen
    </li>
    <li>
      <b>Implementierung</b> in einer Programmiersprache
    </li>
  </ol>
</Proof>

# Laufzeitanalyse von Algorithmen

<Definition title="Wachstumverhalten">
  Die folgenden Wachstumsklassen für eine Funktion $f$:
  {`$$\\begin{aligned}
  \\forall c \\in \\N^+ &\\text{gilt} \\\\
  O(g) &= \\cset{f \\mid f \\leq c*g} \\\\
  o(g) &= \\cset{f \\mid f < c*g} \\\\
  \\Omega(g) &= \\cset{f \\mid c^{-1}*g \\leq f} \\\\
  \\omega(g) &= \\cset{f \\mid c^{-1}*g < f} \\\\
  \\Theta(g) &= \\Omega(g) \\cap O(g)
  \\end{aligned}$$`}
  $\Theta$ ist eine genaue Abschätzung
  <Proof>
    <span>
      {`$$
    \\underset{\\text{untere Schranke}}{\\boxed{c_1*g(n)}} \\leq
    f(n) \\leq
    \\underset{\\text{obere Schranke}}{\\boxed{c_2*g(n)}}
    $$`}
    </span>
    <br />
    <span>{`$$\\Omega(g) \\leq f(n) \\leq O(g)$$`}</span> <br />
    <span>{`$$\\omega(g) < f(n) < o(g)$$`}</span>
  </Proof>
</Definition>

| Name          |    Laufzeit     |
| :------------ | :-------------: |
| Konstant      |     $O(1)$      |
| Logarithmisch |   $O(log(n))$   |
| Linear        |     $O(n)$      |
| N LOG N       | $O(n * log(n))$ |
| Quadratisch   |    $O(n^2)$     |
| Kubisch       |    $O(n^3)$     |
| Exponentiell  |    $O(k^n)$     |

## Master-Theorem

<Definition title="Master-Theorem">
  Die Laufzeit $T(n)$ eines Divide-And-Conquer Algorithmus kann mit einer
  Ungleichung abgeschätzt werden
  {`$$T(n) \\leq a * T(\\ceil{\\frac{n}{b}}) + O(n^c)$$`}
  {`Das Master-Theorem ergibt sich für $1 \\leq a,1 < b$ und $0 \\leq c,d$`}
  <ul>
    <li>a : Anzahl der Teilprobleme</li>
    <li>b : Größe der Teilprobleme</li>
    <li>c : Lineare Schritte des Zusammenfügens</li>
    <li>d : Logarithmische Schritte des Zusammenfügens</li>
  </ul>
  {`$$T(n) = \\underbrace{a \\cdot T(\\frac{n}{b})}_{\\text{Teilen}}+\\underbrace{\\Theta(n^c \\cdot \\log^{d}(n))}_{\\text{Zusammenfügen}}$$`}
  <Proof>
    {`$$\\begin{aligned}
1 < \\frac{b^c}{a} & \\implies \\Theta(n^{\\log_b(a)}) \\\\
\\frac{b^c}{a} = 1 & \\implies \\Theta(n^c * \\log(n)) \\\\
\\frac{b^c}{a} < 1 & \\implies \\Theta(n^c)
\\end{aligned}$$`}
  </Proof>
</Definition>
<Example title="Master-Theorem (Binäre Suche)">
  {`a = 1, weil nur ein Teilproblem betrachtet wird`}
  {`b=2, weil wir in jedem Schritt den Suchraum teilen`}
  {`c=0 und d=0, weil kein Aufwand beim Zusammenfügen`}
  {`$$\\frac{b^c}{a} = \\frac{2^0}{1}=1$$`}
  {`$$T(n) \\in \\Theta(n^0 * \\log(n)^{0+1} * n) = \\Theta(\\log(n))$$`}
</Example>

# Greedy-Algorithmen und Scheduling-Probleme

<Problem>
  <Problem.Name>Intervall-Scheduling-Problem</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Folge $R=r_1,r_2,\dots,r_n$ von Jobs</li>
      <li>
        Ein Job $r_i$ soll zum Zeitpunkt $s(i)$ gestartet werden und dann bis
        $f(i)$ ausgeführt werden
      </li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    <b>Maximierungsproblem</b>
    <br />
    Bestimme ein <i>zulässiges</i> Schedule maximaler Größe ohne Konflikte.
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>
        Zwei Jobs $r_i,r_j \in R: i \neq j$ stehen in Konflikt gdw. sie sich
        überlappen $[s(i),f(i)[$ und $[s(j),f(j)[$
      </li>
      <li>Jobs mit $f(i) = s(j)$ stehen nicht in Konflikt</li>
      <li>
        Optimale Lösungsstrategien:
        <ul>
          <li>
            <b>Earliest-Finishing-First</b> $O(n*\log(n))$
          </li>
          <li>
            <b>Latest-Starttime-First</b> $O(n*\log(n))$
          </li>
        </ul>
      </li>
    </ul>
  </Problem.Notes>
</Problem>
<Definition title="Greedy Algorithmus">
  <i>Greedy</i> Algorithmen zeichnen sich dadurch aus, dass sie iterativ eine
  globale Lösung konstruieren indem sie sich mit jedem Schritt verbessern.
  Greedy Algorithmen können optimal sein, müssen sie aber nicht. Lösungen und
  Teillösungen werden mit <b>Heuristiken</b> bewertet.
</Definition>
<Problem>
  <Problem.Name>Intervall-Partitionierung-Problem</Problem.Name>
  <Problem.Input>Folge $R=r_1,\dots,r_n$ von Jobs</Problem.Input>
  <Problem.Question>
    <b>Minimierungsproblem</b> <br />
    Verwende möglichst wenige Maschinen um alle Jobs konfliktfrei auszuführen.
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>
        Wenn Jobs in Konflikt stehen, dann gibt es eine Überlappung, dann muss
        neue Maschine belegt werden
      </li>
      <li>
        Optimale Lösung
        <ul>
          <li>
            <b>Starttime-Ordering</b> $O(n*\log(n))$
          </li>
        </ul>
      </li>
    </ul>
  </Problem.Notes>
</Problem>
<Problem>
  <Problem.Name>Scheduling mit Deadlines</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Folge $R=r_1,\dots,r_n$ von Jobs</li>
      <li>Startpunkt $t$, Dauer $\tau(i)$, Deadline $\delta(i)$</li>
      <li>Job $r_i$ endet $f(i) = t + \tau(i)$</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    <b>Minimierungsproblem</b> <br />
    Alle Jobs auf einer Maschine ausführen ohne Konflikte, sodass Summe der Verspätungen
    minimiert wird
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>Job $r_i$ erfüllt Deadline gdw. $f(i) \leq \delta(i)$</li>
      <li>sonst Verspätung von $v(i)=f(i)-\delta(i)$</li>
      <li>
        Optimale Lösung
        <ul>
          <li>
            <b>Earliest-Deadline-First</b> $O(n*\log(n))$
          </li>
        </ul>
      </li>
    </ul>
  </Problem.Notes>
</Problem>
<Problem>
  <Problem.Name>Caching-Problem</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Anfragesequenz $X=x_1,\dots$</li>
      <li>Cache $C$ mit Größe $k$</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Konstruiere ein Schedule, sodass möglichst wenige Cache-Misses auftreten
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>Cache-Miss wenn $x_i$ angefragt und nicht in Cache</li>
      <li>sonst Cache-hit</li>
      <li>Bei LOAD(x) und Cache voll, muss EVICT(y) ausgeführt werden</li>
      <li>
        Operationen:
        <ul>
          <li>LOAD(x): Element $x_i$ wird geladen - Kosten 1</li>
          <li>EVICT(x): Element $x_j$ wird aus Cache entfernt - Kosten 0</li>
        </ul>
      </li>
      <li>
        Optimale Lösung
        <ul>
          <li>
            (offline) <b>Evict-Furthest-In-Future</b> $O(n*k)$
          </li>
        </ul>
      </li>
    </ul>
  </Problem.Notes>
</Problem>
<Problem>
  <Problem.Name>Gewichtetes Intervall-Scheduling</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Folge $R=r_1,\dots,r_n$ von Jobs</li>
      <li>Ausführung von Job $r_i$ gibt $w_i$ Gewinn</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    <b>Maximierungsproblem</b> <br />
    Maximiere die Summe der Gewichte / Gesamtgewinn
  </Problem.Question>
  <Problem.Notes>
    <b>Lösung mit Dynamischer Programmierung</b> <br />
    {`$$W_{i+1} = \\max{\\cset{W_i,W_{p(i+1)}+w_{i+1}}}$$`}
    <ul>
      <li>Das Gewicht $W_i$ beschreibt die Summe der ersten $i$ Jobs</li>
      <li>{`$p(i+1)$ beschreibt den größten Index $j \\leq i$, sodass die Jobs $r_j$ und $r_{i+1}$ keinen Konflikt haben`}</li>
      <li>{`$w_{i+1}$ ist das Gewicht des Jobs an Stelle i+1`}</li>
    </ul>
    <span>{`Wenn Job $r_{i+1}$ Teil der optimalen Lösung OPT ist, dann können nur noch die Jobs in OPT sein die nicht in Konflikt mit $r_{i+1}$ stehen. Dieser wird bestimmt durch $p(i+1)$`}</span>
  </Problem.Notes>
</Problem>
<Problem>
  <Problem.Name>Scheduling mit fester Deadline</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Eine Menge von Jobs $R$</li>
      <li>Laufzeiten $\tau(i)$</li>
      <li>Globale Deadline $\Delta$</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Finde ein Schedule für 2 Maschinen, sodass alle Jobs bis zur Deadline
    $\Delta$ fertig sind.
  </Problem.Question>
</Problem>
<Problem>
  <Problem.Name>Bin-Packing</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Anzahl von Behältern (Bins) mit Volumen $k$</li>
      <li>Anzahl von Objekten mit Gewichten</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Können alle Objekte auf die Bins verteilt werden, sodass keiner überläuft?
  </Problem.Question>
  <Problem.Notes>NP-Vollständig</Problem.Notes>
</Problem>
<Problem>
  <Problem.Name>k-Partition</Problem.Name>
  <Problem.Input>Eine Menge $X$ von Zahlen</Problem.Input>
  <Problem.Question>
    Kann $X$ in $k$ Teilmengen unterteilt werden, sodass die Summen aller
    Teilmengen gleich ist?
  </Problem.Question>
  <Problem.Notes>NP-Vollständig</Problem.Notes>
</Problem>
<Problem>
  <Problem.Name>k-Graph-Coloring</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Graph $G=(V,E)$</li>
      <li>Natürliche Zahl $k$</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    <b>Entscheidungsproblem</b> <br />
    Existiert für den Graph $G$ eine <i>zulässige</i> Färbung mit höchstens $k$ Farben?
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>
        Färbung heißt <i>zulässig</i> wenn, keine benachbarten Knoten die
        gleiche Farbe haben
      </li>
      <li>Es gibt $n \choose k$ Möglichkeiten</li>
      <li>
        Optimale Greedy-Strategie: Farben sind geordnet, jedem Knoten wird die
        Farbe zugeordnet mit dem kleinsten Index oder neue Farbe erstellt und
        verteilt.
      </li>
    </ul>
  </Problem.Notes>
</Problem>

# Dynamische Programmierung

<Problem>
  <Problem.Name>Kunstgalerie</Problem.Name>
  <Problem.Input>
    <ul>
      <li>$2n$ Räume</li>
      <li>Ein Raum hat Popularität {`$w_{i,j}$`}</li>
      <li>
      Räume dürfen nur geschlossen werden, wenn alle offenen Räume erreichbar bleiben </li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    <b>Maximierungsproblem</b> <br />
    $k$ Räume müssen geschlossen werden, sodas Popularität maximal bleibt.
  </Problem.Question>
</Problem>
<Example title="Kunstgalerie">
  <ul>
    <li>$k$... Anzahl Räume die noch geschlossen werden müssen</li>
    <li>$i$... Ebene des Raums</li>
    <li>
      $j$... Optionen in akt. Ebene
      <ul>
        <li>0 = beide offen</li>
        <li>1 = oben offen</li>
        <li>2 = unten offen</li>
      </ul>
    </li>
    <li>$w$ Popularität des Raums</li>
    <li>
      {`$T[i,j,k]$`} - Ein Tripel der für Ebene $i$ das Maximum aller
      Möglichkeiten betrachtet:
      <ul>
        <li>Beide Räume offen lassen und alle Möglichkeiten für Ebene $i-1$</li>
        <li>
          Einen Raum schließen und eingeschränkte Möglichkeiten für Ebene $i-1$
        </li>
      </ul>
    </li>
    <li>
      {`$$
    T[i,j,k] = \\begin{cases} 
    \\underset{a \\in \\cset{0,1,2}}{\\max} & 
    \\underset{\\text{beide offen}}{\\boxed{(T[i-1,a,k] + w_{i,0} + w_{i,1})}}
    \\\\ \\\\
    \\underset{b \\in \\cset{0,1}}{\\max} & 
    \\underset{\\text{oben offen - unten schließen}}{\\boxed{(T[i-1,b,k-1] + w_{i,0})}}
    \\\\ \\\\
    \\underset{c \\in \\cset{0,2}}{\\max} & 
    \\underset{\\text{unten offen - oben schließen}}{\\boxed{(T[i-1,c,k-1] + w_{i,1})}}
    \\end{cases}$$`}
    </li>
    <li>Gesamtergebnis ist {`$\\max \\cset{T[n,0,k],T[n,1,k],T[n,2,k]}$`}</li>
  </ul>
</Example>
<Problem>
  <Problem.Name>Longest Common Subsequence</Problem.Name>
  <Problem.Input>
    <ul>
      <li>2 Strings $X,Y$ mit $|X|=n$ und $|Y|=m$</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Bestimme längste Teilfolge $Z$ die sowohl in $X$ und in $Y$ vorkommt.
  </Problem.Question>
  <Problem.Notes>
    <b>Lösung mit Dynamischer Programmierung</b> <br />
    {`$$M[i,j] = \\begin{cases}
    M[i\\!-\\!1,j\\!-\\!1] \\!+\\! 1 & x_i = y_j \\\\
    \\max(M[i,j\\!-\\!1],M[i\\!-\\!1,j]) & x_i \\neq y_j
    \\end{cases}
    $$`}
    <ul>
      <li>$M[0,j]$ und $M[i,0]$ werden auf 0 gesetzt</li>
      <li>
        Eine Zelle $M[i,j]$ beschreibt Länge der LCS von $x_1 \dots x_i$ und
        $y_1 \dots y_j$
      </li>
      <li>Länge LCS steht in $M[n,m]$</li>
    </ul>
  </Problem.Notes>
</Problem>
<Problem>
  <Problem.Name>String-Alignment</Problem.Name>
  <Problem.Input>
    <ul>
      <li>2 Strings $X,Y$ mit $|X|=n$ und $|Y|=m$</li>
      <li>Aktionen (Insert, Update, Delete)</li>
      <li>Kosten für Aktionen</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Bestimme Levenshtein-Distanz von $X$ und $Y$
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>
        Levenshtein-Distanz = Minimale Summe der Kosten aller benötigten
        Aktionen
      </li>
    </ul>
  </Problem.Notes>
</Problem>
<Problem>
  <Problem.Name>Subset-Sum</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Menge natürlicher Zahlen $M$</li>
      <li>Zahl $S$ (Summe)</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Kann die Summe $S$ mit Zahlen der Menge $M$ gebildet werden?
  </Problem.Question>
  <Problem.Notes>NP-Schwer</Problem.Notes>
</Problem>
<Definition title="Pseudopolynomialzeit">
  Pseudopolynomialzeit sieht auf den ersten Blick polynomiell aus, aber wenn die
  Laufzeit abhängig ist von einer festen Zahl dann kann die Laufzeit doch
  exponentiell sein.
</Definition>

# Graphen und Wege-Probleme

> Für Graphen Definitionen siehe <Link to="other/Graphentheorie">Graphentheorie</Link>

<Problem>
  <Problem.Name>Single Source Shortest Path</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Graph $G=(V,E)$ mit Gewichten</li>
      <li>Startknoten $u$</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Finde den kürzesten Weg von $u$ zu allen anderen Knoten
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>
        <b>Keine negativen Kantengewichte</b>: Dijkstra
      </li>
      <li>
        <b>Keine negativen Kreise</b>: Bellman-Ford
      </li>
    </ul>
  </Problem.Notes>
</Problem>

```python
def bellman_ford(graph, start):
  V, E = graph
  n = len(V)
  dist, predecessor = [], []

  for vertex in V:
    dist[vertex] = math.inf
    predecessor[vertex] = None

  dist[start] = 0
  # In an acyclic graph no vertex is visited twice
  # hence n-1 iterations
  for i in range(n-1):  # O(|V|-1)
    for (u,v,w) in E:   # O(|E|)
      # dist holds shortest distance
      # from source to u
      if dist[u] + w < dist[v]:
        dist[v] = dist[u] + w
        predecessor[v] = u

  for (u,v,w) in E: # O(|E|)
    if dist[u] + w < dist[v]:
      raise Error('Graph contains negative cycle')

  return dist, predecessor  # O(|V|*|E|)
```

## Fluss-Netzwerke

<Definition title="Netzwerk">
  Ein Netzwerk ist ein gerichteter Graph $G=(V,E,\gamma,q,s)$ wobei
  <ul>
    <li>$\gamma$ Kapazitätsfunktion für eine Kante (wieviel kann fließen)</li>
    <li>Quelle $q$ (Eingrad 0)</li>
    <li>Senke $s$ (Ausgrad 0)</li>
  </ul>
</Definition>
<Definition title="Flow">
  Ein Fluss $f$ für eine Kante in einem Netzwerk kann nicht größer der Kapazität
  $\gamma$ sein.
  <ul>
    <li>
      Mit Ausnahme der Quelle und Senke alles was in einen Knoten fließt auch
      wieder herausfließen.
    </li>
    <li>
      $W(f)$ bezeichnet die Summe aller Flüsse in einem Netzwerk ausgehend von
      der Quelle $q$.
    </li>
    <li>{`$\\texttt{MAXFLOW(G)}$`} bezeichnet den maximalen Fluss.</li>
    <li>
      Für jedes Netzwerk gilt {`$\\texttt{MAXFLOW(G)}=\\texttt{MINCUT(G)}$`}
    </li>
  </ul>
</Definition>
<Definition title="Cut">
  Für ein Netzwerk ist ein $q|s$-Schnitt eine Zerlegung der Knoten in zwei
  disjunkte Teilmengen $Q,S$.
  <ul>
    <li>
      Die <b>Schnittkapazität</b> ist die Summe aller Kanten von $Q$ nach $S$
      <br />
      (Rückfluss von $S$ nach $Q$ wird nicht berücksichtigt)
    </li>
    <li>{`$\\texttt{MINCUT(G)}$`} bezeichnet einen minimalen $q|s$-Schnitt</li>
    <li>
      Nettofluss={`$\\sum f(v_Q,u_S) - \\sum f(u_S,v_Q) \\leq \\gamma(Q,S)$`}
    </li>
  </ul>
</Definition>
<Definition title="Restgraph">
  Der Restgraph {`$\\texttt{REST(G,f)}$`} für einen Fluss $f$ wird so
  konstruiert, dass
  <ul>
    <li>
      Für jede Kante $e$ aus $G$ mit Fluss $f(e)$ und Kapazität $\gamma(e)$
      <ul>
        <li>
          Falls $f(e) \lt \gamma(e)$ füge Vorwärtskante $e'$ in Restgraph mit
          Kapazität $\gamma(e')=\gamma(e)-f(e)$ (<i>was noch Fließen könnte</i>)
        </li>
        <li>
          Falls $f(e) \gt 0$ (positiver Fluss) füge eine Rückwärtskante $e''$
          mit der gleichen Kapazität $f(e'')=f(e)$ ein
        </li>
      </ul>
    </li>
    <li>
      Weg von Quelle zu Senke im Restgraphen heißt <b>zunehmender Weg</b>
    </li>
  </ul>
  <Image imgName="Restgraph-ex.png" />
</Definition>

```pseudocode
General-Maxflow(Netzwerk G)
  Restgraph R = Nullfluss (Fluss für alle Kanten = 0)
  while (es gibt einen zunehmenden Weg p in R) {
    Wähle zunehmenden Weg p
    Berechne minimum der verbleibenden Kapazitäten in p
    for ((u,v) in p) {
      Erhöhe Fluss f(u,v) um minimum
      Verringere Fluss f(v,u) um minimum
    }
  }
```

## Matchings

<Problem>
  <Problem.Name>2-Dimensionales Matching</Problem.Name>
  <Problem.Input>
    <ul>
      <li>ungerichteter Graph $G=(V,E)$</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Gesucht wird eine maximale Teilmenge $E' \subseteq E$ von Kanten für die
    alle Kantenpaare paarweise disjunkt sind. Also kein Knoten indiziert mit
    mehr als einer Kante aus $E'$.
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>
        Ein Matching heißt <b>perfekt</b> gdw. alle Knoten mit einer Kante aus
        $E'$ inzident sind
      </li>
    </ul>
  </Problem.Notes>
</Problem>
<Problem>
  <Problem.Name>3-Dimensionales Matching</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Menge $M$ von Tripeln über Grundmengen $A \times B \times C$</li>
      <li>$A,B,C$ sind paarweise disjunkt</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Gesucht wird eine Teilmenge von $M' \subseteq M$ in der jedes Element aus $A
    \cup B \cup C$ höchstens einmal vorkommt.
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>NP-Vollständig</li>
    </ul>
  </Problem.Notes>
</Problem>

# Dynamische Datenstrukturen und abstrakte Datentypen

## Prioritätswarteschlange

<Definition title="Priority Queue">
  Eine Prioritätswarteschlange ist ein abstrakter Datentyp die eine Menge von
  Elementen verwaltet. Jedem Element wird eine Priorität natürlicherOrdnung
  zugewiesen. <br />
  <b>Unterstützte Operationen:</b>
  <ul>
    <li>
      <b>Max</b>: finde Element höchster Priorität
    </li>
    <li>
      <b>Insert, Delete</b>
    </li>
    <li>
      <b>UpdatePriority</b>
    </li>
  </ul>
</Definition>

## Binomialbaum

<Definition title="Binomialbaum">
  Ein Binomialbaum $B_n$ hat
  <ul>
    <li>Wurzel mit $n$ Kindern</li>
    <li>$2^n$ Knoten</li>
    <li>Höhe $n$</li>
    <li>
      <i>kann</i> als zwei {`$B_{n-1}$`} dargestellt werden, wobei eine Wurzel
      der anderen Wurzel als Wurzel dient
    </li>
    <li>mehrere Binomialbäume werden als binomialer Wald bezeichnet</li>
  </ul>
  <b>Unterstützte Operationen:</b>
  <ul>
    <li>
      <b>Max</b>: finde Element höchster Priorität
    </li>
    <li>
      <b>Insert, Delete</b>
    </li>
    <li>
      <b>Merge</b>: Zwei Binomialbäume werden verschmolzen
    </li>
  </ul>
  <Image imgName="B-Trees-ex.png" />
</Definition>
<Definition title="Tries">
  Tries sind Bäume die besonders gut geeignet sind für das Speichern von Strings
  über ein Alphabet $\Sigma$. Jeder Knoten hat einen Grad $d \leq |\Sigma|$.
</Definition>

## Union-Find

<Definition title="Union-Find">
  Der Union-Find Datentyp verwaltet Elemente als disjunkte Mengen.
  Suchoperationen (FIND) können die interne Struktur dynamisch ändern, sodass
  ein Element in zukünftigen Anfragen schneller findbar ist. <br />
  <b>Unterstützte Operationen:</b>
  <ul>
    <li>
      <b>Make-Set(x)</b>: erstellt eine Menge mit $x$
    </li>
    <li>
      <b>Union(x,y)</b>: Vereinigung der Mengen die $x$ und $y$ enthalten
    </li>
    <li>
      <b>Find(x)</b>: Finden den Repräsentation/Identifier der Menge mit $x$
    </li>
  </ul>
</Definition>
<Example title="Union-Find">
  <Codeblock language="python">
    {`for i in range(6):
  Make-Set(i)
Union(1,2)
Union(3,4)
Union(1,4)
Union(2,6)
Find(4)`}
  </Codeblock>
  <div style="margin: 0; display: flex;">
    <div style="flex: 1">
      <Image imgName="Union-Find-ex.png" />
    </div>
    <div style="flex: 1">
      <Image imgName="Algo9-Wald.png" />
    </div>
  </div>
</Example>

### Darstellung

<Proof>
  <b>Verkettete Liste</b>
  <ul>
    <li>Jede Menge wird als verkettete Liste dargestellt</li>
    <li>Element an 1. Stelle ist Repräsentant</li>
    <li>Repräsentant zeigt auf das letzte Element</li>
    <li>Alle Elemente zeigen auf Repräsentant (auch Repräsentant)</li>
  </ul>
  <b>Union(x,y)</b>
  Eine (kürzere von beiden) wird an die andere Liste gehangen. $n$ Zeiger müssen
  geupdatet werden und der Zeiger von Repr. auf das letzte Element.
</Proof>
<Proof>
  <b>Wald</b>
  <ul>
    <li>Jeder Knoten zeigt auf Elternknoten</li>
    <li>Wurzeln sind Repräsentanten</li>
  </ul>
  <b>Union(x,y)</b>
  Der Repräsentant von $y$ wird als Kindknoten an den Repräsentanten von $x$ gehangen
  <b>Pfadkompression</b>
  Bei einer Find(x) Anfrage wird der Zeiger auf den Elternknoten direkt auf den Repräsentanten
  umgehangen, sodass zukünftige Find(x) Operationen weniger Zeit kosten (O(1) statt
  O(n=Tiefe des Baums))
</Proof>

## Hashing

<Definition title="Hashfunktion">
  Eine Hashfunktion $h(x)$ für eine Hashtabelle der Größe $k$ bildet ein Element
  auf einen Index $i \leq k$ ab.
  <Example>$h(x) = 2x+7 \mod k$</Example>
  <ul>
    <li>
      <b>Kollision</b>: Mehrere Elemente werden auf den gleichen Wert abgebildet
    </li>
    <li>
      <b>Universellle</b> Hashfunktionen haben eine Kollisionswahrscheinlichkeit
      von kleiner-gleich {`$\\frac{1}{k}$`}
    </li>
  </ul>
</Definition>

### Konfliktbehandlung

<Definition title="Überlaufliste">
  Wenn ein Element auf einen Platz im Cache abgebildet wird der schon belegt
  ist, dann kann eine Überlaufliste angelegt werden und das einzufügende Element
  wird dann an den nächsten freien Platz in der Liste eingefügt. Suchoperationen
  durchsuchen dann auch die Überlauflisten.
</Definition>
<Definition title="Sondierungsfolge">
  Bei einem Konflikt kann ein Element in einen anderen freien Platz im Cache
  gesetzt werden, welcher durch eine sog. Sondierungsfolge bestimmt wird.
  <ul>
    <li>
      <b>Lineares Sondieren</b>: Nächster freier Platz in einer Richtung
    </li>
    <li>
      <b>Quadratisches Sondieren</b>: Nächster freier Platz in beiden Richtungen
    </li>
    <li>
      <b>Double Hashing</b>:Zweite Hashingfunktion abhängig von der Anzahl von
      Konflikten
    </li>
    <li>
      <b>Cuckoo-Hashing</b>: 2 unterschiedliche Hashfunktionen. Falls es zwei
      Konflikte gibt, wird eines der in Konflikt stehenden Element verdrängt und
      für jenes erneut per Hashfunktion eingefügt. Achtung es können Zyklen
      entstehen.
    </li>
  </ul>
</Definition>

## Bloom-Filter

<Definition title="Bloom-Filter">
  Der Cache enthält nur Bits. Bis zu $m$ Hashfunktionen werden benutzt um die
  nötigen Bits für ein Element zu setzen
  <b>Unterstützte Operationen:</b>
  <ul>
    <li>
      <b>Insert</b>: Anhand der Hashfunktionen werden alle Bits gesetzt für ein
      Element.
    </li>
    <li>
      <b>Search</b>: Search schaut ob alle Bits an den Positionen gesetzt sind.
      Negative Ergebnisse sind immer korrekt, positive können aber falsch sein
    </li>
    <li>
      <b>Delete</b>: Nicht möglich, Suche korrekt ist
    </li>
  </ul>
</Definition>

# Online Probleme und Algorithmen

<Definition title="Online-Problem">
  Ein Algorithmus für ein Online-Problem kann seine Berechnung nur abhängig von
  der Vergangenheit und der Gegenwart tätigen, weil die Zukunft für
  Online-Probleme unbekannt ist.
</Definition>
<Definition title="Kompetitive Rate">
  Sei {`$\\texttt{ALG}(X)$`} eine Lösung für ein beliebiges Optimierungsproblem
  und {`$\\texttt{OPT}(X)$`} eine optimale Lösung mit Eingabe $X$. Die
  kompetitive Rate $k$ beschreibt wie gut {`$\\texttt{ALG}$`} für Eingabe $X$
  abschneidet.
  {`$$k = \\frac{\\texttt{cost}_X(\\texttt{ALG})}{\\texttt{cost}_X(\\texttt{OPT})}$$`}
  <ul>
    <li>Für $k=1$ ist {`$\\texttt{ALG}$`} optimal für $X$</li>
    <li>
      Für alle Eingaben $X$
      <ul>
        <li>
          $k$-kompetitiv <b>Minimierungsproblem</b>:
          {`$$\\texttt{ALG}(X) \\leq k * \\texttt{OPT}(X) + b$$`}
        </li>
        <li>
          $k$-kompetitiv <b>Maximierungsproblem</b>:
          {`$$\\frac{\\texttt{OPT}(X)}{k} - b \\leq \\texttt{ALG}(X)$$`}
        </li>
        <li>stark $k$-kompetitiv, wenn $b=0$</li>
      </ul>
    </li>
  </ul>
</Definition>
<Problem>
  <Problem.Name>Skifahrer-Problem</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Kaufpreis $K$</li>
      <li>Leihpreis $L \lt K$</li>
      <li>unbekannte Sequenz von Saisons</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    <b>Minimierungsproblem</b> <br />
    Es ist unbekannt ob in der nächsten Saison Ski gefahren wird, für die aktuelle
    Saison muss entschieden werden ob Ausrüstung geliehen ($L$) oder gekauft ($K$)
    werden soll.
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>
        Vor jeder Saison muss Ski-Ausrüstung geliehen ($L$) oder gekauft $(K)$.{" "}
      </li>
      <li>
        <b>2-Kompetitive Strategie</b>
        <ul>
          <li>
            {`$\\frac{L}{K}-1$`} Saisons leihen und in der nächsten kaufen.
          </li>
          <li>
            <b>kompetitive Rate</b>{" "}
            {`$$\\frac{xL+K}{\\min \\cset{(x+1)L,K}} \\leq 2$$`}
          </li>
          <li>Worst Case: Letzte Saison kommt direkt nachdem gekauft wurde</li>
        </ul>
      </li>
    </ul>
  </Problem.Notes>
</Problem>

## Online Caching-Problem

<Example title="First-In-First-Out">
  Ersetzt das Element, welches am längsten im Cache ist
</Example>
<Example title="Last-In-First-Out">
  Ersetzt das Element, welches am kürzesten im Cache ist
</Example>
<Example title="Evict-Least-Frequently-Used">
  Ersetzt das Element, welches am wenigsten oft benutzt wurde
</Example>
<Example title="Evict-Least-Recently-Used">
  Ersetzt das Element, welches am längsten nicht angefragt wurde.
  <i>k-kompetitiv</i>
</Example>
<Example title="Flush-When-Full">
  Bei Cache-Miss wird Cache vollständig geleert
</Example>
<Definition title="Markierungsalgorithmus">
  In einer Phase der Länge $k$ befinden sich höchstens $k$ unterschiedliche
  Elemente. Ein Algorithmus $A$ für Cache-Größe $k$ erfüllt die{" "}
  <b>Markierungs-Eigenschaft</b>, wenn folgendes für jedes $X$ gilt:
  <ol>
    <li>
      Zu Beginn jeder Phase $X^i$ einer $k$-Phasen-Partition von $X$ werden alle
      Elemente, die $A$ zu diesem Zeitpunkt im Cache eingelagert hat,
      unmarkiert.{" "}
    </li>
    <li>
      Sobald ein Element in $X^i$ auftaucht, muss $A$ sicherstellen, dass es
      sich im Cache befindet, und es wird markiert.
    </li>
    <li>$A$ entfernt niemals ein markiertes Element aus seinem Cache</li>
  </ol>
  Jeder Markierungsalgorithmus hat eine stark kompetitive von
  {`$$\\frac{k}{(k-h+1)}$$`}, wobei $h \leq k$ die Größe des Caches für einen optimalen
  Algorithmus ist.
</Definition>

## Listenzugriffsproblem

<Problem>
  <Problem.Name>Listenzugriffsproblem</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Liste $L=a_1,a_2,\dots,a_n$ der Länge $n$</li>
      <li>Request-Sequenz $X$ die Elemente aus der Liste erfragt</li>
      <li>
        <b>Aktionen: Transposition</b>
        <ul>
          <li>Tauschen mit direktem Vorgänger umsonst (frei)</li>
          <li>Tauschen mit direktem Nachfolger kostenpflichtig</li>
        </ul>
      </li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    <b>Minimierungsproblem</b> <br />
    Finde eine Strategie um die Elemente mit Hilfe von Transpositionen umzuordnen,
    sodass die Kosten für die Anfrage-Sequenz minimiert werden.
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>
        Die Anfrage $x$ für ein Element an der $k$-ten Stelle verursacht Kosten
        $k$
      </li>
      <li>Alle angefragten Elemente sind in $L$</li>
      <li>Länge der Liste verändert sich nicht</li>
    </ul>
  </Problem.Notes>
</Problem>
<Example title="Move to Front">
  Das angefragte Element wird an den Anfang der Liste bewegt (ohne Kosten).
  <ul>
    <li>
      <b>Worst-Case Eingabe</b> <br />
      Immer das letzte Element in der Liste anfragen
    </li>
    <li>kompetitive Rate {`$\\geq 2-\\frac{1}{|L|}$`}</li>
  </ul>
</Example>
<Example title="Transpose">
  Das angefragte Element wird nur mit seinem direktem Vorgänger getauscht (ohne
  Kosten).
  <ul>
    <li>
      <b>Worst-Case Eingabe</b> <br />
      Immer das letzte Element in der Liste anfragen
    </li>
    <li>
      <b>nicht</b> kompetitive Rate {`$\\geq \\frac{2}{3}|L|-o(1)$`}
    </li>
  </ul>
</Example>
<Example title="Frequency Count">
  Die Elemente werden nach absteigender Häufigkeit bisheriger Zugriffe sortiert.
  <ul>
    <li>
      <b>Worst-Case Eingabe</b>
      {`$$X_w = [
    \\underbrace{a_1,\\dots,a_1}_{\\text{m}},
    \\underbrace{a_2,\\dots,a_2}_{\\text{m-1}},
    \\dots,
    \\underbrace{a_n,\\dots,a_n}_{\\text{m-n-1}}
  ]$$`}
    </li>
    <li>
      <b>nicht</b> kompetitive Rate {`$\\geq \\frac{|L|+1}{2}-o(1)$`}
    </li>
  </ul>
</Example>

## Laufzeitanalyse cont.

<Definition title="Worst-Case-Analyse">
  Die Worst-Case-Analyse betrachtet keine Umstände.
</Definition>
<Definition title="Potentialfunktion $\varPhi$">
  Die Potentialfunktion berechnet das Potential der Datenstruktur zu einem
  gewissen Zeitpunkt $t$ ungeachtet von der Folge von Operationen wie sie diesen
  Zustand erreicht hat.
  <ul>
    <li>Es bietet sich (oft) an $\varPhi(S) = |S|$ zu setzen</li>
    <li>{`$$\\underbrace{a_i}_{\\text{Amortisierte Kosten}} = \\underbrace{c_i}_{\\text{richtige Kosten}} + \\underbrace{\\varPhi(S_i)}_{\\text{Potential nach i}} - \\underbrace{\\varPhi(S_{i-1})}_{\\text{Potential vor i}}$$`}</li>
    <li>{`$$A = (\\sum_{i}^{n}{c_i}) + \\varPhi(n) - \\varPhi(0)$$`}</li>
    <li>
      Ist die Potentialfunktion nie negativ und anfangs Null, so sind die realen
      Gesamtkosten höchstens die amortisierten Gesamtkosten.
    </li>
  </ul>
  <Example title="Stack">
    Wir definieren das Potential des Stacks $S$ als die Anzahl der Elemente
    <ul>
      <li>{`$\\Phi(S) = |S|$`}</li>
      <li>{`$\\Phi(S_i)=\\Phi(S_{i-1}) \\pm c_i$`}</li>
      <li>
        Kosten für
        <ul>
          <li>{`$c_{\\text{push}}=1$`}</li>
          <li>{`$c_{\\text{pop}}=1$`}</li>
          <li>{`$c_{\\text{multi-pop}}=\\min \\cset{|S_i|, k}$`}</li>
        </ul>
      </li>
      <li>Push vergrößert das Potential des Stacks um 1</li>
      <li>Pop verringert das Potential des Stacks um 1</li>
      <li>Multi-Pop verringert das Potential des Stacks um $k \leq |S|$</li>
    </ul>
    <Proof>
      {`$$\\begin{aligned}
    a_{\\text{push}} &= c_{\\text{push}} + \\Phi(S_i) - \\Phi(S_{i-1}) \\\\
    &= 1 + (\\cancel{\\boxed{\\Phi(S_{i-1})}} + 1) \\cancel{\\boxed{-\\Phi(S_{i-1})}} \\\\
    &= 2 \\\\
    a_{\\text{pop}} &= c_{\\text{pop}} + \\Phi(S_i) - \\Phi(S_{i-1}) \\\\
    &= 1 + (\\cancel{\\boxed{\\Phi(S_{i-1})}} - 1) \\cancel{\\boxed{-\\Phi(S_{i-1})}} \\\\
    &= 0\\\\
    a_{\\text{multi-pop}} &= c_{\\text{multi-pop}} + \\Phi(S_i) - \\Phi(S_{i-1}) \\\\
    &= \\min \\cset{|S_i|, k} + (- \\min \\cset{|S_i|, k}) \\\\
    &= 0
    \\end{aligned}$$`}
    </Proof>
    Mit Hilfe dieser Potentialfunktion können wir die Schranke $O(n)$ finden, weil
    die amortisierten Kosten im Worst-Case ($n$ Push-Operationen) genau $2n$ betragen
    und $O(2n) \in O(n)$ liegt.
  </Example>
</Definition>
<Problem>
  <Problem.Name>K-Server Problem</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Menge von Punkten $M$</li>
      <li>Abstandsfunktion $d$ für die Punkte in $M$</li>
      <li>$k$ mobile Server</li>
      <li>Anfragesequenz von Punkten</li>
      <li>
        Aktionen
        <ul>
          <li>Server von $u$ nach $v$ mit Kosten $d(u,v)$ bewegen</li>
        </ul>
      </li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    <b>Minimierungsproblem</b> <br />
    Bediene alle Anfragen mit minimalen Kosten
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>Jede Anfrage muss von einem Server bedient werden</li>
      <li>Eine Anfrage heißt bedient, wenn ein Server im Punkt $x_i$ ist</li>
      <li>Alle Server können sich gleichzeitig bewegen</li>
      <li>
        Konfigurationsraum
        <ul>
          <li>
            Mehrere Server gleicher Punkt : {`$m^k$`}
            <ul>
              <li>Server eindeutig : {`$\\frac{m^k}{k!}$`}</li>
            </ul>
          </li>
          <li>Ein Server pro Punkt : {`$\\binom{m}{k}$`}</li>
        </ul>
      </li>
    </ul>
  </Problem.Notes>
</Problem>
<Proof>
  <b>Double-Cover Strategie für K-Server</b>
  <span>
    Falls der Punkt in der Mitte von zwei Servern liegt, dann werden beide
    Server gleichmäßig zum Punkt bewegt.
  </span>
  <span>
    Falls beide Server auf der gleichen Seite des Punktes liegen, wird nur einer
    bewegt.
  </span>
  <span>
    Diese Strategie ist <b>k-kompetitiv</b> im eindimensionalen Euklidschen
    Raum.
  </span>
</Proof>

# Approximations-Algorithmen

<Definition title="Approximationsrate">
  Ein Approximationsalgorithmus für ein Minimierungsproblem heißt
  $\alpha$-approximativ falls für eine feste Konstante $b$ und alle Eingaben $X$
  gilt
  <li>
    Für alle Eingaben $X$
    <ul>
      <li>
        $\alpha$-approximativ <b>Minimierungsproblem</b>:
        {`$$\\texttt{ALG}(X) \\leq \\alpha * \\texttt{OPT}(X) + b$$`}
      </li>
      <li>
        $\alpha$-approximativ <b>Maximierungsproblem</b>:
        {`$$\\frac{\\texttt{OPT}(X)}{\\alpha} - b \\leq \\texttt{ALG}(X)$$`}
      </li>
      <li>stark $\alpha$-approximativ, wenn $b=0$</li>
    </ul>
  </li>
</Definition>
<Problem>
  <Problem.Name>Load-Balancing Problem</Problem.Name>
  <Problem.Input>
    <ul>
      <li>$n$ Jobs $J_1,\dots,J_n$</li>
      <li>Ausführungszeiten $t(J_i)$</li>
      <li>$k$ Maschinen $M_1,\dots,M_k$</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    <b>Minimierungsproblem</b> <br />
    Alle Jobs sollen so ausgeführt werden, dass die Gesamtlaufzeit minimal ist.
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>NP-Schwer</li>
      <li>Die Gesamtlaufzeit ist das Maximum der Laufzeiten pro Maschine</li>
    </ul>
  </Problem.Notes>
</Problem>
<Example title="Earliest-Free-Machine">
  Verteile den nächsten Job auf die Maschine, die als erstes wieder frei ist.
  <ul>
    <li>
      <b>Worst-Case Eingabe</b>
      {`$$J = [
        \\underbrace{\\text{Jobs Länge 1}}_{(k-1)*k*n}, 
        \\underbrace{\\text{Job Länge } k*n}_{1}
      ]$$`}
    </li>
    <li>
      <b>Approximative Rate</b>
      {`$$2-\\frac{1}{k}$$`}
    </li>
    <li>
      Verbesserung: Jobs absteigend nach Ausführungszeiten sortieren
      {`$$\\frac{3}{2}-\\frac{1}{2k}$$`}
    </li>
  </ul>
</Example>
<Problem>
  <Problem.Name>Min-Set-Cover</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Universum $U$ wird unterteilt</li>
      <li>Folge $A=A_1,\dots,A_n$ überdeckt $U$</li>
      <li>Jedes $A_i$ verursacht Kosten $w(A_i) \geq 0$</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Gesucht wird eine Teilfolge $B=B_1,\dots,B_m$ von $A$, sodass die
    Vereinigung aller Teilfolgen das Universum $U$ überdeckt.
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li>NP-Schwer</li>
    </ul>
  </Problem.Notes>
</Problem>
<Example title="Greedy-Set-Cover">
  <ul>
    <li>
      <i>Marginalkosten</i> für $A_i$ sind Kosten pro Element
      {`$$\\frac{w(A_i)}{|A_i|}$$`}
    </li>
    <li>
      <i>relative Marginalkosten</i> für $A_i$ sind Kosten pro nicht abgedecktem
      Element
      {`$$\\frac{w(A_i)}{|A_i \\cap \\underbrace{R \\subseteq U}_{\\text{Rest}}|}$$`}
    </li>
  </ul>
  <Codeblock>
    {`while (Universum noch nicht abgedeckt) {
  Wähle Teilmenge Bj mit minimalen rel. Marginalkosten
}`}
  </Codeblock>
</Example>
<Problem>
  <Problem.Name>Min-Vertex-Cover</Problem.Name>
  <Problem.Input>
    <ul>
      <li>Graph $G=(V,E)$</li>
    </ul>
  </Problem.Input>
  <Problem.Question>
    Gesucht wird eine minimale Teilmenge von Knoten $V' \subseteq V$, sodass für
    alle Kanten $(u,v) \in E$ mindestens ein Knoten in $V'$ ist ($u \in V' \lor
    v \in V'$).
  </Problem.Question>
  <Problem.Notes>
    <ul>
      <li></li>
    </ul>
  </Problem.Notes>
</Problem>

# Randomisierte Algorithmen

# Diskrete Optimierungsprobleme
