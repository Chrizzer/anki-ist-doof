import React from "react"
import "./style.css"

export default data => {
  return (
    <>
      <div className="category">
        <span className="description">{data.name || 'Empty'}</span>
      </div>
    </>
  )
}
