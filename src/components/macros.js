export const customMacros = {
  "\\csum": `\\sum_{#1}^{#2}`,
  "\\cset": `\\{#1\\}`,
  "\\b": `\\textbf{#1}`,
  "\\i": `\\textit{#1}`,
  "\\ceil": `\\left \\lceil #1 \\right \\rceil`,
  "\\floor": `\\left \\lfloor #1 \\right \\rfloor`
}
