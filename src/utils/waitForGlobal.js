// GOD BLESS https://github.com/mreishus for his solution!
// https://github.com/hanai/gatsby-remark-mathjax/issues/1#issuecomment-443436362
export default function waitForGlobal(name, timeout = 300) {
  return new Promise((resolve, reject) => {
    let waited = 0

    function wait(interval) {
      setTimeout(() => {
        waited += interval
        // some logic to check if script is loaded
        // usually it something global in window object
        if (window[name] !== undefined) {
          return resolve()
        }
        if (waited >= timeout * 1000) {
          return reject({ message: "Timeout" })
        }
        wait(interval * 2)
      }, interval)
    }
    wait(30)
  })
}
